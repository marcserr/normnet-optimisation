import sys
import os
import networkx as nx
import matplotlib.pyplot as plt
sys.path.append(os.getcwd().replace('/Graphics', ''))
import PARAMETERS


class ProblemGraphVisualizer:

    def __init__(self, file_path = None, visualize = True, out_file = None, format = "PNG", resultFile = None):
        self.graph = nx.DiGraph()
        self.pos = {}
        self.goods = []
        self.roots = []
        self.colors = []
        if os.path.exists(file_path):
            self.loadData(file_path)
            self.generatePositions()
            self.generateColors(resultFile)
            self.draw(out_file = out_file, format = format, show = visualize)


    @staticmethod
    def replaceBids():
        for i in range(1,4):
            for j in range(1,4):
                for b in [25, 50, 75]:
                    for round in range(10):
                        filepath = os.getcwd()+"/5000N150G/STRUCT/STRUCT_"+str(i)+"_"+str(j)+"_50g_"+str(b)+"b_"+str(round)+".txt"
                        structFile = open(filepath, "r")
                        writeFilepath = filepath+".tmp"
                        writeFile = open(writeFilepath, "w")
                        for line in structFile:
                            if "Bids" not in line:
                                writeFile.write(line)
                            else:
                                writeFile.write(line.replace("Bids", "Goods"))
                        os.rename(filepath, filepath+".old")
                        os.rename(writeFilepath, writeFilepath.replace(".tmp", ""))
                        structFile.close()
                        writeFile.close()
                        os.remove(filepath+".old")

    def loadData(self, filepath):
        if os.path.exists(filepath):
            structFile = open(filepath, "r")
            lookGraphStruct = False
            lookBidsStruct = False
            for line in structFile:
                if lookBidsStruct:
                    node, goods = line.split(':')
                    goods = goods.strip('\n').split(',')
                    for good in goods:
                        self.graph.add_edge(int(node), "g"+str(int(good)+1))
                if line[:5] == "Goods":
                    num_goods = line[5:].strip(' ').strip('\n').strip('(').strip(')')
                    lookGraphStruct = False
                    lookBidsStruct = True
                    for good in range(1,int(num_goods)+1):
                        nodename = "g"+str(good)
                        self.graph.add_node(nodename)
                        self.goods.append(nodename)
                if lookGraphStruct:
                    node, childs = line.split(":")
                    childs = childs.strip('\n').split(',')
                    node = int(node)
                    if "" in childs:
                        childs.remove("")
                    self.graph.add_node(node)
                    for child in childs:
                        self.graph.add_node(int(child))
                    for child in childs:
                        self.graph.add_edge(int(child), node)
                if line.strip('\n') == "Graph structure":
                    lookGraphStruct = True
            for node in self.graph.nodes():
                if self.isNorm(node):
                    self.roots.append(node)
            num_nodes = len(self.roots)
            for node in range(num_nodes):
                for generalized in self.getGeneralizedNorms(node):
                    if self.isNorm(generalized):
                        self.roots.remove(generalized)
            structFile.close()
        else:
            print "ERROR: The path: " + filepath + " does not exist."

    def isGood(self, node):
        if isinstance(node, int):
            ret = False
        else:
            if isinstance(node, str):
                ret = True
            else:
                ret = False
        return ret

    def isNorm(self, node):
        return not self.isGood(node)

    def getNumLayers(self):
        num_layers = 2
        toExplore = []
        for root in self.roots:
            toExplore.append((root, 2))
        while toExplore:
            (node, layer) = toExplore.pop(0)
            if layer > num_layers:
                num_layers = layer
            for child in self.getGeneralizedNorms(node):
                toExplore.append((child, layer+1))
        return num_layers

    def generatePositions(self):
        num_layers = self.getNumLayers()
        good_id = 1
        for g in self.goods:
            self.givePosition(g, 1, num_layers, nodes_layer=len(self.goods), node_number=good_id)
            good_id += 1
        root_id = 1
        toExplore = []
        for r in self.roots:
            self.givePosition(r, 2, num_layers, nodes_layer=len(self.roots), node_number=root_id)
            root_id += 1
            for child in self.getGeneralizedNorms(r):
                toExplore.append((child, r , 3))
        while toExplore:
            (node, parent, layer) = toExplore.pop(0)
            parent_of_parent = None
            brothers = self.getGeneralizedNorms(parent)
            num_brothers = len(brothers)
            num_child = None

            for edge in self.graph.edges():
                if edge[0] == parent and self.isNorm(edge[1]):
                    parent_of_parent = edge[1]

            if not isinstance(parent_of_parent, int):
                parent_dist = 1.0/float(len(self.roots))
            else:
                parent_dist = 1.0/float(len(self.roots))
                for brother in self.getGeneralizedNorms(parent_of_parent):
                    if brother != parent:
                        parent_dist = min(parent_dist, abs(self.pos[parent][0]-self.pos[brother][0]))

            for i in range(num_brothers):
                if brothers[i] == node:
                    num_child = i+1

            self.givePosition(node, layer, num_layers, self.pos[parent][0], parent_dist, num_brothers, num_child)

            for child in self.getGeneralizedNorms(node):
                toExplore.append((child, node, layer+1))


    def getGeneralizedNorms(self, node):
        generalised = []
        for edge in self.graph.edges():
            if edge[1] == node:
                generalised.append(edge[0])
        return generalised


    def givePosition(self, node, layer, num_layers, parent_x_pos = None, parent_layer_dist = None, parents_childs = None, child_number = None, nodes_layer = None, node_number= None):
        ypos = 1-((1.0/float(num_layers))*float(layer)-(1.0/float(num_layers))/2.0)

        if nodes_layer:
            xpos = (1.0/float(nodes_layer))*float(node_number)-(1.0/float(nodes_layer))/2.0
        else:
            initialPos = parent_x_pos-float(parent_layer_dist)/2.0
            finalPos = parent_x_pos+float(parent_layer_dist)/2.0
            xposRange = finalPos-initialPos
            xpos = initialPos+(float(xposRange)/float(parents_childs))*child_number-(float(xposRange)/float(parents_childs))/2.0

        self.pos[node] = [xpos, ypos]

    def draw(self, out_file = None, format = "PNG", show = True):
        nx.draw_networkx(self.graph, self.pos, node_color = self.colors, with_labels = True)
        if out_file:
            plt.savefig(out_file, format)
        if show:
            plt.show()

    def generateColors(self, resultFile):
        if resultFile and os.path.exists(resultFile):
            normsSelected = []
            nodesSelected = []
            data = open(resultFile, "r")
            look = False
            for line in data:
                if line.strip("\n") == " </variables>":
                    look = False
                if look:
                    variablename =line.strip("\n").replace(" ", "") [15:].split("\"")[0]
                    if variablename!= "y":
                        norm = int(line[2:].strip("\n").split(" ")[2][7:].split("\"")[0])
                        selected = int(line[2:].strip("\n").split(" ")[3][7:].split("\"")[0])
                        if selected:
                            nodesSelected.append(int(norm))
                if line.strip('\n') == " <variables>":
                    look = True
            for norm in nodesSelected:
                actualCount = 0
                queue = range(len(self.roots)-1, -1, -1)
                appendPos = len(queue)
                while queue:
                    currentNorm = queue.pop()
                    if actualCount == norm:
                        normsSelected.append(currentNorm)
                    if currentNorm in self.roots:
                        appendPos = len(queue)
                    for child in self.getGeneralizedNorms(currentNorm):
                        queue.insert(appendPos, child)
                    actualCount += 1

            for node in self.graph.nodes():
                if node in normsSelected:
                    self.colors.append("cyan")
                else:
                    self.colors.append("red")
        else:
            for i in range(len(self.graph.nodes())):
                self.colors.append("red")

    @staticmethod
    def easyLoad(num_norms, num_goods, height, width, budget, gen, s_round, repower):
        if PARAMETERS.GRAPH_PROBLEM_TYPE == "VMNSPLB":
            gorv = "V"
        else:
            gorv = "G"
        structureFile = "STRUCT_"+str(height)+"_"+str(width)+"_"+str(gen)+"g_"+str(budget)+"b_"+str(s_round)+".txt"
        structureDirectory = os.getcwd()+"/"+str(num_norms)+"N"+str(num_goods)+gorv+"/STRUCT/"
        filePath = structureDirectory+structureFile
        if PARAMETERS.SHOW_SOLUTION:
            resultDirectory = os.getcwd()+"/"+str(num_norms)+"N"+str(num_goods)+"G/RESULTS/"
            resultFilename = "RESULT"+str(repower)+"_"+str(num_norms)+"N_"+str(height)+"_"+str(width)+"_"+str(gen)+"g_"+str(budget)+"b_"+str(s_round)+".txt"
            resultFilePath = resultDirectory+resultFilename
        else:
            resultFilePath = None
        ProblemGraphVisualizer(filePath, resultFile=resultFilePath)

    @staticmethod
    def loadFromParameters():
        ProblemGraphVisualizer.easyLoad(PARAMETERS.GRAPH_NORMS, PARAMETERS.GRAPH_GOODS, PARAMETERS.GRAPH_HEIGHT, PARAMETERS.GRAPH_WIDTH, PARAMETERS.GRAPH_BUDGET, PARAMETERS.GRAPH_GEN, PARAMETERS.GRAPH_ROUND, PARAMETERS.GRAPH_REPOWER)


def main():
    ProblemGraphVisualizer.loadFromParameters()

if __name__ == "__main__":
    main()
