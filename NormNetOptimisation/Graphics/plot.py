from GraphicPlotter import GraphicPlotter
import os
import PARAMETERS

def main():
    goodsorvalues = 'G'
    if PARAMETERS.EXPERIMENTS_WITH_VALUES:
        goodsorvalues = 'V'
    plot_data_file = os.getcwd().replace('Graphics', 'PLOTDATA/')+str(PARAMETERS.NUM_NORMS_PLOT)+'N'+str(PARAMETERS.NUM_GOODS_PLOT)+goodsorvalues+'/plot'+str(PARAMETERS.NUM_NORMS_PLOT)+'N'+str(PARAMETERS.NUM_GOODS_PLOT)+goodsorvalues+'.txt'

    gp = GraphicPlotter(plot_data_file)

    if PARAMETERS.PLOT_TYPE == 1:
        gp.plot()
    elif PARAMETERS.PLOT_TYPE == 2:
        gp.plot3d()
    elif PARAMETERS.PLOT_TYPE == 3:
        if PARAMETERS.FIX_BUDGET:
            budgets = PARAMETERS.BUDGET
        else:
            budgets = [1,2,3]
        if PARAMETERS.FIX_GEN:
            gens = PARAMETERS.GEN
        else:
            gens = [0.25, 0.5, 0.75]
        if PARAMETERS.FIX_REPOWS:
            repows = PARAMETERS.REPOWS
        else:
            repows = ['S', 'I']
        for b in budgets:
            for g in gens:
                for r in repows:
                    gp.plotHeightWidth(r, b, g)
    elif PARAMETERS.PLOT_TYPE == 4:
        if PARAMETERS.FIX_BUDGET:
            budgets = PARAMETERS.BUDGET
        else:
            budgets = [1,2,3]
        if PARAMETERS.FIX_GEN:
            gens = PARAMETERS.GEN
        else:
            gens = [0.25, 0.5, 0.75]
        if PARAMETERS.FIX_REPOWS:
            repows = PARAMETERS.REPOWS
        else:
            repows = ['S', 'I']
        for g in gens:
            for r in repows:
                gp.plot2d(r, budgets, g)
    elif PARAMETERS.PLOT_TYPE == 5:
        if PARAMETERS.FIX_BUDGET:
            budgets = PARAMETERS.BUDGET
        else:
            budgets = [1,2,3]
        if PARAMETERS.FIX_GEN:
            gens = PARAMETERS.GEN
        else:
            gens = [0.25, 0.5, 0.75]
        if PARAMETERS.FIX_REPOWS:
            repows = PARAMETERS.REPOWS
        else:
            repows = ['S', 'I']
        for g in gens:
            for r in repows:
                gp.plot2dscatter(r, budgets, g)
    elif PARAMETERS.PLOT_TYPE==6:
        if PARAMETERS.FIX_BUDGET:
            budgets = PARAMETERS.BUDGET
        else:
            budgets = [1,2,3]
        if PARAMETERS.FIX_GEN:
            gens = PARAMETERS.GEN
        else:
            gens = [0.25, 0.5, 0.75]
        if PARAMETERS.FIX_REPOWS:
            repows = PARAMETERS.REPOWS
        else:
            repows = ['S', 'I']
        for g in gens:
            for r in repows:
                gp.plot2dscatterValues(r, budgets, g)
    elif PARAMETERS.PLOT_TYPE == 7:
        gp.showDataFromPath(PARAMETERS.PATH1, PARAMETERS.PATH2)
    else:
        print "INVALID PLOT TYPE"

if __name__ == "__main__":
    main()
