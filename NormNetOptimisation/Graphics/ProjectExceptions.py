class IncongruentGraphException(Exception):
    pass

class IncongruentProbabilitiesException(Exception):
    pass
