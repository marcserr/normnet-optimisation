import os
import time
import cplex

def problem(p,budget,problem_lp, problem_sol, timeLim, height, width, gen, plotfile = 'plot.txt'):

    #Receives a LP file and solves it. It exports the answer to a .sol file and also shows it.
    #You can specify the time limit with timeLim.

    try:


        m=cplex.Cplex(problem_lp)

        m.parameters.timelimit.set(timeLim)
        m.parameters.mip.limits.treememory.set(2800)
        m.parameters.threads.set(1)
        start_time = time.time()
        m.solve()
        final_time = time.time()-start_time

        m.solution.write(problem_sol)

        n = len(m.solution.get_values())

        if os.path.exists(plotfile):
            f = open(plotfile, 'a')
        else:
            f = open(plotfile, 'w')

        lp_file = open(problem_lp, 'r')
        res_file = open(problem_sol, 'r')
        num_rel = 0
        num_ex_rel = None
        num_gen_rel = None
        look = False
        for lp_line in lp_file:

            if lp_line[:30] == "/* # Generalisation relations:":
                num_gen_rel = int(lp_line[30:].strip('\n').replace('*/', ''))
            elif lp_line[:27] == "/* # Exclusivity relations:":
                num_ex_rel = int(lp_line[27:].strip('\n').replace('*/', ''))

            if lp_line == "Binaries\n":
                look = False
            if look:
                data = lp_line.strip('\n')
                data = data[len(data)-1]
                if data == '1':
                    num_rel += 1
            if lp_line == "Subject To\n":
                look = True

        look = False
        sol_norms = 0
        for res_line in res_file:
            if res_line == " </variables>\n":
                look = False
            if look:
                data = res_line.strip('\n')
                data = data[2:].replace('<', '').replace('>', '').replace('/', '').replace('\"', '')
                data = data.split(' ')
                if data[3][6] == '1':
                    sol_norms += 1
            if res_line == " <variables>\n":
                look = True

        try:
            f.write(p+','+str(budget)+','+str(height)+','+str(width)+','+str(gen)+','+str(n-1)+','+str(final_time)+','+str(sol_norms)+','+str(num_ex_rel)+','+str(num_gen_rel)+'\n')
        except:
            print "Unable to write plot data for case: "+p+","+str(budget)+","+str(height)+","+str(width)+","+str(gen)
        f.close()
        print "Solved in "+str(final_time)+" s."

    except cplex.exceptions.CplexError:
        print "Sorry"
