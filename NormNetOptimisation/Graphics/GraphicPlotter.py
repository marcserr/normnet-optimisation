import sys
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import random
from GP import GP
from scipy.interpolate import UnivariateSpline
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
import os
import PARAMETERS
import GaussP

class GraphicPlotter:
    """

    """

    def __init__(self, plotfile):
        self.N = 3
        self.plotfile = plotfile
        data = plotfile.split('/')
        data = data[len(data)-2]
        data = data.replace('N', ' ').replace('G', '').split(' ')
        self.num_norms = data[0]
        self.num_goods = data[1]

    def loadData(self):
        size = os.path.getsize(self.plotfile)
        myFile = open(self.plotfile, "r")
        self.G1 = []
        self.G2 = []
        self.G3 = []
        self.I1 = []
        self.I2 = []
        self.I3 = []
        while size:
            line = myFile.readline()
            data = line.strip('\n').split(',')
            if data[0] == 'G':
                if int(data[1]) == 1:
                    self.G1.append( float(data[6] ) )
                elif int(data[1]) == 2:
                    self.G2.append( float(data[6] ) )
                else:
                    self.G3.append(float(data[6]))
            else:
                if int(data[1]) == 1:
                    self.I1.append(float(data[6]))
                elif int(data[1]) == 2:
                    self.I2.append(float(data[6]))
                else:
                    self.I3.append(float(data[6]))
            size -= len(line)
        myFile.close()

    def plot(self):
        self.loadData()
        G1 = self.interQuartile(self.G1)
        G2 = self.interQuartile(self.G2)
        G3 = self.interQuartile(self.G3)
        I1 = self.interQuartile(self.I1)
        I2 = self.interQuartile(self.I2)
        I3 = self.interQuartile(self.I3)
        gen_means = (self.estimator(G1),self.estimator(G2),self.estimator(G3))
        gen_std = (np.std(G1),np.std(G2),np.std(G3))
        
        ind = np.arange(self.N)  # the x locations for the groups
        width = 0.35       # the width of the bars
        
        fig, ax = plt.subplots()
        rects1 = ax.bar(ind, gen_means, width, color='r', yerr=gen_std)

        incl_means = (self.estimator(I1),self.estimator(I2),self.estimator(I3))
        incl_std = (np.std(I1),np.std(I2),np.std(I3))
        
        rects2 = ax.bar(ind + width, incl_means, width, color='y', yerr=incl_std)
        
        ax.set_ylabel('Times')
        ax.set_title('Times by budget and power')
        ax.set_xticks(ind + width / 2)
        ax.set_xticklabels(('Low budget', 'Medium budget', 'High budget'))
        
        ax.legend((rects1[0], rects2[0]), ('G', 'I'))
        self.autolabel(rects1,ax)
        self.autolabel(rects2, ax)
        
        print gen_means
        print gen_std
        print incl_means
        print incl_std

        figpath = self.plotfile[:len(self.plotfile)-4]+'_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+'.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()

    def loadData3d(self):
        size = os.path.getsize(self.plotfile)
        myFile = open(self.plotfile, "r")
        self.G1 = []
        self.G2 = []
        self.G3 = []
        self.I1 = []
        self.I2 = []
        self.I3 = []
        self.G11 = []
        self.G12 = []
        self.G13 = []
        self.G21 = []
        self.G22 = []
        self.G23 = []
        self.G31 = []
        self.G32 = []
        self.G33 = []
        self.I11 = []
        self.I12 = []
        self.I13 = []
        self.I21 = []
        self.I22 = []
        self.I23 = []
        self.I31 = []
        self.I32 = []
        self.I33 = []
        while size:
            line = myFile.readline()
            data = line.strip('\n').split(',')
            if data[0] == 'G':
                if int(data[1]) == 1:
                    self.G1.append( (float(data[6]), int(data[7]), int(data[8])) )
                elif int(data[1]) == 2:
                    self.G2.append( (float(data[6]), int(data[7]), int(data[8])) )
                else:
                    self.G3.append((float(data[6]), int(data[7]), int(data[8])))
            else:
                if int(data[1]) == 1:
                    self.I1.append((float(data[6]), int(data[7]), int(data[8])))
                elif int(data[1]) == 2:
                    self.I2.append((float(data[6]), int(data[7]), int(data[8])))
                else:
                    self.I3.append((float(data[6]), int(data[7]), int(data[8])))
            size -= len(line)
        myFile.close()
        self.distribute()

    def loadAllData(self, repow, budget, gen):
        self.H1W1 = []
        self.H1W2 = []
        self.H1W3 = []
        self.H2W1 = []
        self.H2W2 = []
        self.H2W3 = []
        self.H3W1 = []
        self.H3W2 = []
        self.H3W3 = []
        self.H1W1norms = []
        self.H1W2norms = []
        self.H1W3norms = []
        self.H2W1norms = []
        self.H2W2norms = []
        self.H2W3norms = []
        self.H3W1norms = []
        self.H3W2norms = []
        self.H3W3norms = []
        self.H1W1exrels = []
        self.H1W2exrels = []
        self.H1W3exrels = []
        self.H2W1exrels = []
        self.H2W2exrels = []
        self.H2W3exrels = []
        self.H3W1exrels = []
        self.H3W2exrels = []
        self.H3W3exrels = []
        self.H1W1genrels = []
        self.H1W2genrels = []
        self.H1W3genrels = []
        self.H2W1genrels = []
        self.H2W2genrels = []
        self.H2W3genrels = []
        self.H3W1genrels = []
        self.H3W2genrels = []
        self.H3W3genrels = []
        size = os.path.getsize(self.plotfile)
        myFile = open(self.plotfile, "r")
        while size:
            line = myFile.readline()
            data = line.strip('\n').split(',')
            if data[0] == repow and data[1] == str(budget) and data[4] == str(gen):
                if data[2] == '1':
                    if data[3] == '1':
                        self.H1W1.append(float(data[6]))
                        self.H1W1norms.append(int(data[7]))
                        self.H1W1exrels.append(float(data[8]))
                        self.H1W1genrels.append(float(data[9]))
                    elif data[3] == '2':
                        self.H1W2.append(float(data[6]))
                        self.H1W2norms.append(int(data[7]))
                        self.H1W2exrels.append(float(data[8]))
                        self.H1W2genrels.append(float(data[9]))
                    else:
                        self.H1W3.append(float(data[6]))
                        self.H1W3norms.append(int(data[7]))
                        self.H1W3exrels.append(float(data[8]))
                        self.H1W3genrels.append(float(data[9]))
                elif data[2] == '2':
                    if data[3] == '1':
                        self.H2W1.append(float(data[6]))
                        self.H2W1norms.append(int(data[7]))
                        self.H2W1exrels.append(float(data[8]))
                        self.H2W1genrels.append(float(data[9]))
                    elif data[3] == '2':
                        self.H2W2.append(float(data[6]))
                        self.H2W2norms.append(int(data[7]))
                        self.H2W2exrels.append(float(data[8]))
                        self.H2W2genrels.append(float(data[9]))
                    else:
                        self.H2W3.append(float(data[6]))
                        self.H2W3norms.append(int(data[7]))
                        self.H2W3exrels.append(float(data[8]))
                        self.H2W3genrels.append(float(data[9]))
                else:
                    if data[3] == '1':
                        self.H3W1.append(float(data[6]))
                        self.H3W1norms.append(int(data[7]))
                        self.H3W1exrels.append(float(data[8]))
                        self.H3W1genrels.append(float(data[9]))
                    elif data[3] == '2':
                        self.H3W2.append(float(data[6]))
                        self.H3W2norms.append(int(data[7]))
                        self.H3W2exrels.append(float(data[8]))
                        self.H3W2genrels.append(float(data[9]))
                    else:
                        self.H3W3.append(float(data[6]))
                        self.H3W3norms.append(int(data[7]))
                        self.H3W3exrels.append(float(data[8]))
                        self.H3W3genrels.append(float(data[9]))
            size -= len(line)
        myFile.close()
        self.interQuartile(self.H1W1, erase=True)
        self.interQuartile(self.H1W2, erase=True)
        self.interQuartile(self.H1W3, erase=True)
        self.interQuartile(self.H2W1, erase=True)
        self.interQuartile(self.H2W2, erase=True)
        self.interQuartile(self.H2W3, erase=True)
        self.interQuartile(self.H3W1, erase=True)
        self.interQuartile(self.H3W2, erase=True)
        self.interQuartile(self.H3W3, erase=True)
        self.interQuartile(self.H1W1norms, erase=True)
        self.interQuartile(self.H1W2norms, erase=True)
        self.interQuartile(self.H1W3norms, erase=True)
        self.interQuartile(self.H2W1norms, erase=True)
        self.interQuartile(self.H2W2norms, erase=True)
        self.interQuartile(self.H2W3norms, erase=True)
        self.interQuartile(self.H3W1norms, erase=True)
        self.interQuartile(self.H3W2norms, erase=True)
        self.interQuartile(self.H3W3norms, erase=True)
        self.interQuartile(self.H1W1exrels, erase=True)
        self.interQuartile(self.H1W2exrels, erase=True)
        self.interQuartile(self.H1W3exrels, erase=True)
        self.interQuartile(self.H2W1exrels, erase=True)
        self.interQuartile(self.H2W2exrels, erase=True)
        self.interQuartile(self.H2W3exrels, erase=True)
        self.interQuartile(self.H3W1exrels, erase=True)
        self.interQuartile(self.H3W2exrels, erase=True)
        self.interQuartile(self.H3W3exrels, erase=True)
        self.interQuartile(self.H1W1genrels, erase=True)
        self.interQuartile(self.H1W2genrels, erase=True)
        self.interQuartile(self.H1W3genrels, erase=True)
        self.interQuartile(self.H2W1genrels, erase=True)
        self.interQuartile(self.H2W2genrels, erase=True)
        self.interQuartile(self.H2W3genrels, erase=True)
        self.interQuartile(self.H3W1genrels, erase=True)
        self.interQuartile(self.H3W2genrels, erase=True)
        self.interQuartile(self.H3W3genrels, erase=True)

    def plotHeightWidth(self, repow, budget, gen):
        self.loadAllData(repow, budget, gen)
        time_means = GraphicPlotter.eraseNaNData(((self.estimator(self.H1W1), self.estimator(self.H1W2), self.estimator(self.H1W3)),(self.estimator(self.H2W1), self.estimator(self.H2W2), self.estimator(self.H2W3)),(self.estimator(self.H3W1), self.estimator(self.H3W2), self.estimator(self.H3W3))))
        time_std = GraphicPlotter.eraseNaNData(((np.std(self.H1W1), np.std(self.H1W2), np.std(self.H1W3)),(np.std(self.H2W1), np.std(self.H2W2), np.std(self.H2W3)),(np.std(self.H3W1), np.std(self.H3W2), np.std(self.H3W3))))
        norms_means = GraphicPlotter.eraseNaNData(((self.estimator(self.H1W1norms), self.estimator(self.H1W2norms), self.estimator(self.H1W3norms)),(self.estimator(self.H2W1norms), self.estimator(self.H2W2norms), self.estimator(self.H2W3norms)),(self.estimator(self.H3W1norms), self.estimator(self.H3W2norms), self.estimator(self.H3W3norms))))
        norms_std = GraphicPlotter.eraseNaNData(((np.std(self.H1W1norms), np.std(self.H1W2norms), np.std(self.H1W3norms)),(np.std(self.H2W1norms), np.std(self.H2W2norms), np.std(self.H2W3norms)),(np.std(self.H3W1norms), np.std(self.H3W2norms), np.std(self.H3W3norms))))
        exrels_means = GraphicPlotter.eraseNaNData(((self.estimator(self.H1W1exrels), self.estimator(self.H1W2exrels), self.estimator(self.H1W3exrels)),(self.estimator(self.H2W1exrels), self.estimator(self.H2W2exrels), self.estimator(self.H2W3exrels)),(self.estimator(self.H3W1exrels), self.estimator(self.H3W2exrels), self.estimator(self.H3W3exrels))))
        exrels_std = GraphicPlotter.eraseNaNData(((np.std(self.H1W1exrels), np.std(self.H1W2exrels), np.std(self.H1W3exrels)),(np.std(self.H2W1exrels), np.std(self.H2W2exrels), np.std(self.H2W3exrels)),(np.std(self.H3W1exrels), np.std(self.H3W2exrels), np.std(self.H3W3exrels))))
        genrels_means = GraphicPlotter.eraseNaNData(((self.estimator(self.H1W1genrels), self.estimator(self.H1W2genrels), self.estimator(self.H1W3genrels)),(self.estimator(self.H2W1genrels), self.estimator(self.H2W2genrels), self.estimator(self.H2W3genrels)),(self.estimator(self.H3W1genrels), self.estimator(self.H3W2genrels), self.estimator(self.H3W3genrels))))
        genrels_std = GraphicPlotter.eraseNaNData(((np.std(self.H1W1genrels), np.std(self.H1W2genrels), np.std(self.H1W3genrels)),(np.std(self.H2W1genrels), np.std(self.H2W2genrels), np.std(self.H2W3genrels)),(np.std(self.H3W1genrels), np.std(self.H3W2genrels), np.std(self.H3W3genrels))))
        fig = plt.figure()
        fig.suptitle(PARAMETERS.ESTIMATOR.capitalize()+' time of resolution, '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\nQ = '+str(PARAMETERS.QUARTILE_CUT)+' Budget = '+str(budget)+' GenProb = '+str(gen))
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('1', '2', '3'))
            ax.set_xlabel('Width', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('1', '2', '3'))
            ax.set_ylabel('Height', labelpad = 20)
            ys = time_means[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Time', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'_3D_'+'H-W_'+str(repow)+'_'+PARAMETERS.ESTIMATOR+'_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()
        plt.close(fig)
        fig = plt.figure()
        fig.suptitle('Standard deviation time of resolution, '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\nQ = '+str(PARAMETERS.QUARTILE_CUT)+' Budget = '+str(budget)+' GenProb = '+str(gen))
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('1', '2', '3'))
            ax.set_xlabel('Width', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('1', '2', '3'))
            ax.set_ylabel('Height', labelpad = 20)
            ys = time_std[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Time', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'_3D_'+'H-W_'+str(repow)+'_STD_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()
        plt.close(fig)
        fig = plt.figure()
        fig.suptitle(PARAMETERS.ESTIMATOR.capitalize()+' num norms of resolution, '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\nQ = '+str(PARAMETERS.QUARTILE_CUT)+' Budget = '+str(budget)+' GenProb = '+str(gen))
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('1', '2', '3'))
            ax.set_xlabel('Width', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('1', '2', '3'))
            ax.set_ylabel('Height', labelpad = 20)
            ys = norms_means[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Norms in Solution', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'_3D_'+'NUM-NORMS_'+str(repow)+'_'+PARAMETERS.ESTIMATOR+'_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()
        plt.close(fig)
        fig = plt.figure()
        fig.suptitle('STD num of norms of resolution, '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\nQ = '+str(PARAMETERS.QUARTILE_CUT)+' Budget = '+str(budget)+' GenProb = '+str(gen))
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('1', '2', '3'))
            ax.set_xlabel('Width', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('1', '2', '3'))
            ax.set_ylabel('Height', labelpad = 20)
            ys = norms_std[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Norms in Solution', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'_3D_'+'NUM-NORMS_'+str(repow)+'_STD_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()
        plt.close(fig)
        fig = plt.figure()
        fig.suptitle(PARAMETERS.ESTIMATOR.capitalize()+' num exclusivity rels of problem, '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\nQ = '+str(PARAMETERS.QUARTILE_CUT)+' Budget = '+str(budget)+' GenProb = '+str(gen))
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('1', '2', '3'))
            ax.set_xlabel('Width', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('1', '2', '3'))
            ax.set_ylabel('Height', labelpad = 20)
            ys = exrels_means[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Relations', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'_3D_'+'NUM-EX-RELS_'+str(repow)+'_'+PARAMETERS.ESTIMATOR+'_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()
        plt.close(fig)
        fig = plt.figure()
        fig.suptitle('STD num exclusivity rels of problem, '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\nQ = '+str(PARAMETERS.QUARTILE_CUT)+' Budget = '+str(budget)+' GenProb = '+str(gen))
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('1', '2', '3'))
            ax.set_xlabel('Width', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('1', '2', '3'))
            ax.set_ylabel('Height', labelpad = 20)
            ys = exrels_std[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Relations', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'_3D_'+'NUM-EX-RELS_'+str(repow)+'_STD_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        plt.close(fig)
        fig = plt.figure()
        fig.suptitle(PARAMETERS.ESTIMATOR.capitalize()+' num generalisation rels of problem, '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\nQ = '+str(PARAMETERS.QUARTILE_CUT)+' Budget = '+str(budget)+' GenProb = '+str(gen))
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('1', '2', '3'))
            ax.set_xlabel('Width', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('1', '2', '3'))
            ax.set_ylabel('Height', labelpad = 20)
            ys = genrels_means[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Relations', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'_3D_'+'NUM-GEN-RELS_'+str(repow)+'_'+PARAMETERS.ESTIMATOR+'_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()
        plt.close(fig)
        fig = plt.figure()
        fig.suptitle('STD num generalisation rels of problem, '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\nQ = '+str(PARAMETERS.QUARTILE_CUT)+' Budget = '+str(budget)+' GenProb = '+str(gen))
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('1', '2', '3'))
            ax.set_xlabel('Width', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('1', '2', '3'))
            ax.set_ylabel('Height', labelpad = 20)
            ys = genrels_std[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Relations', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'_3D_'+'NUM-GEN-RELS_'+str(repow)+'_STD_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()

    def estimator(self, data):
        ret = None
        if PARAMETERS.ESTIMATOR.upper() == "MEAN":
            ret = np.mean(data)
        elif PARAMETERS.ESTIMATOR.upper() == "MEDIAN":
            ret = np.median(data)

        return ret

    def distribute(self):
        if PARAMETERS.DISTRIBUTE_NUM_NORMS:
            pos = 1
        else:
            pos = 2
        principalSets = [self.G1, self.G2, self.G3, self.I1, self.I2, self.I3]
        distributeSets = [[self.G11, self.G12, self.G13], [self.G21, self.G22, self.G23], [self.G31, self.G32, self.G33], [self.I11, self.I12, self.I13], [self.I21, self.I22, self.I23], [self.I31, self.I32, self.I33]]
        for i in range(6):
            main_set = principalSets[i]
            set1 = distributeSets[i][0]
            set2 = distributeSets[i][1]
            set3 = distributeSets[i][2]
            terms = []
            for item in main_set:
                terms.append(item[pos])
            low_bound = np.percentile(terms, 33)
            high_bound = np.percentile(terms, 66)
            for item in main_set:
                if item[pos] <= low_bound:
                    set1.append(item[0])
                elif item[pos] > high_bound:
                    set3.append(item[0])
                else:
                    set2.append(item[0])
            self.interQuartile(set1, erase = True)
            self.interQuartile(set2, erase = True)
            self.interQuartile(set3, erase = True)

    def plot2d(self, repow, budget, gen):
        for type in ["time", "norms"]:
            fig = plt.figure()
            for b in budget:
                self.loadAllData(repow, b, gen)
                if type == "time":
                    data = GraphicPlotter.eraseNaNData(((self.estimator(self.H1W1), self.estimator(self.H1W2), self.estimator(self.H1W3)),(self.estimator(self.H2W1), self.estimator(self.H2W2), self.estimator(self.H2W3)),(self.estimator(self.H3W1), self.estimator(self.H3W2), self.estimator(self.H3W3))))
                else:
                    data = GraphicPlotter.eraseNaNData(((self.estimator(self.H1W1norms), self.estimator(self.H1W2norms), self.estimator(self.H1W3norms)),(self.estimator(self.H2W1norms), self.estimator(self.H2W2norms), self.estimator(self.H2W3norms)),(self.estimator(self.H3W1norms), self.estimator(self.H3W2norms), self.estimator(self.H3W3norms))))
                exrels_means = GraphicPlotter.eraseNaNData(((self.estimator(self.H1W1exrels), self.estimator(self.H1W2exrels), self.estimator(self.H1W3exrels)),(self.estimator(self.H2W1exrels), self.estimator(self.H2W2exrels), self.estimator(self.H2W3exrels)),(self.estimator(self.H3W1exrels), self.estimator(self.H3W2exrels), self.estimator(self.H3W3exrels))))
                genrels_means = GraphicPlotter.eraseNaNData(((self.estimator(self.H1W1genrels), self.estimator(self.H1W2genrels), self.estimator(self.H1W3genrels)),(self.estimator(self.H2W1genrels), self.estimator(self.H2W2genrels), self.estimator(self.H2W3genrels)),(self.estimator(self.H3W1genrels), self.estimator(self.H3W2genrels), self.estimator(self.H3W3genrels))))

                allPairsData = {}

                ratio = []
                pairsdata = []
                i = 0

                while i < len(exrels_means):
                    j = 0
                    ratio.append([])
                    while j < len(exrels_means):
                        r = float(genrels_means[i][j])/float(exrels_means[i][j])
                        ratio[i].append(r)
                        pairsdata.append((ratio[i][j], data[i][j]))
                        j += 1
                    i += 1
                allPairsData[b] = pairsdata

                x = [elem[0] for elem in allPairsData[b]]
                y = [elem[1] for elem in allPairsData[b]]

                GraphicPlotter.sortData(x,y)

                xremove = []
                yremove = []
                for i in range(len(x)):
                    if x[i] < PARAMETERS.SHOW_X_RANGE[0] or x[i] > PARAMETERS.SHOW_X_RANGE[1]:
                        xremove.append(x[i])
                        yremove.append(y[i])

                for elem in xremove:
                    x.remove(elem)
                for elem in yremove:
                    y.remove(elem)

                plt.plot(x,y, label = "Budget "+str(b))

            plt.legend()
            plotfilename = self.plotfile[:len(self.plotfile)-4]+'_2D_LINE_'+type+str(repow)+'_STD_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'
            plt.suptitle(type.capitalize()+', '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\n'' '+str(PARAMETERS.ESTIMATOR)+' Budget = '+str(budget)+' GenProb = '+str(gen))
            if os.path.exists(plotfilename):
                if PARAMETERS.SAVE:
                    plt.savefig(plotfilename)
            if PARAMETERS.SHOW:
                plt.show()
            plt.close(fig)

    def loadScatterPoints(self, type, repow, budget, gen):
        size = os.path.getsize(self.plotfile)
        myFile = open(self.plotfile, "r")
        x = []
        y = []
        pos = None
        if type == "time":
            pos = 6
        elif type == "norms":
            pos = 7
        while size:
            line = myFile.readline()
            data = line.strip('\n').split(',')
            if data[0] == repow and data[1] == str(budget) and data[4] == str(gen):
                x.append(float(data[9])/float(data[8]))
                y.append(float(data[pos]))
            size -= len(line)
        myFile.close()
        if PARAMETERS.SAMPLING and PARAMETERS.SAMPLING < len(x):
            sample = []
            for i in range(PARAMETERS.SAMPLING):
                num = random.choice(range(len(x)))
                while num in sample:
                    num = random.choice(range(len(x)))
                sample.append(num)
            new_x = []
            new_y = []
            for i in sample:
                new_x.append(x[i])
                new_y.append(y[i])
            x = new_x
            y = new_y
        return x, y

    def loadScatterPointsValues(self, type, repow, budget, gen):
        size = os.path.getsize(self.plotfile)
        myFile = open(self.plotfile, "r")
        x = []
        y = []
        pos = None
        if type == "time":
            pos = 6
        elif type == "norms":
            pos = 7
        while size:
            line = myFile.readline()
            data = line.strip('\n').split(',')
            if data[0] == repow and data[1] == str(budget) and data[4] == str(gen):
                x.append(float(data[8]))
                y.append(float(data[pos]))
            size -= len(line)
        myFile.close()
        low = min(x)
        high = max(x)
        for i in range(len(x)):
            x[i] = (x[i]-low)/high

        if PARAMETERS.SAMPLING and PARAMETERS.SAMPLING < len(x):
            sample = []
            for i in range(PARAMETERS.SAMPLING):
                num = random.choice(range(len(x)))
                while num in sample:
                    num = random.choice(range(len(x)))
                sample.append(num)
            new_x = []
            new_y = []
            for i in sample:
                new_x.append(x[i])
                new_y.append(y[i])
            x = new_x
            y = new_y
        return x, y

    def plot2dscatter(self, repow, budget, gen):
        for type in PARAMETERS.WHAT_TO_PLOT:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            fig.suptitle('Effect of the problem structure on the execution time', size=16)
            ax.set_xlabel('Ratio (Generalisation relations / Exclusivity relations)', size=12)
            ax.set_ylabel('Time (sec)', size=12)
            x = []
            y = []
            for b in budget:
                d1, d2 = self.loadScatterPoints(type, repow, b, gen)
                x += d1
                y += d2

            GraphicPlotter.sortData(x,y)

            if type == "time":
                toRemove = []
                upper_cut = np.percentile(y, 100-PARAMETERS.QUARTILE_CUT)
                for i in range(len(x)):
                    if x[i] < PARAMETERS.SHOW_X_RANGE[0] or x[i] > PARAMETERS.SHOW_X_RANGE[1]:
                        toRemove.append(i)
                    if y[i] < PARAMETERS.SHOW_Y_RANGE[0] or y[i] > PARAMETERS.SHOW_Y_RANGE[1]:
                        toRemove.append(i)
                    if y[i] > upper_cut:
                        toRemove.append(i)

                toRemove.sort(reverse=True)

                for i in toRemove:
                    x.pop(i)
                    y.pop(i)

            if PARAMETERS.SHOW_POINTS:
                if PARAMETERS.REGRESSION_LINE:
                    if type == "norms":
                        nans = []
                        for i in range(len(y)):
                            if np.isnan(y[i]):
                                y[i] = 0
                                nans.append(i)
                        w = [1./(float(len(y)-len(nans)))]*len(y)
                        for n in nans:
                            w[n] = 0
                        s = UnivariateSpline(x, y, s=PARAMETERS.REG_DIMENSION, w=w)
                        xs = np.linspace(min(x), max(x), 100)
                        ys = s(xs)
                        plt.plot(xs,ys)
                    else:
                        samples_x = []
                        samples_y = []
                        for el in x:
                            samples_x.append(float(el))
                        for el in y:
                            samples_y.append(float(el))
                        X = np.array(samples_x)
                        Y = np.array(samples_y)
                        GaussP.main(samples_x, X, samples_y, Y)

                else:
                    plt.scatter(x,y, label = "Budget "+str(b))


            if PARAMETERS.SHOW:
                print os.getcwd()
                plt.savefig(os.getcwd())
                plt.show()
            plt.close(fig)

    def plot2dscatterValues(self, repow, budget, gen):
        for type in ["time", "norms"]:
            fig = plt.figure()
            for b in budget:
                x, y = self.loadScatterPointsValues(type, repow, b, gen)

                GraphicPlotter.sortData(x,y)

                if type == "time":
                    toRemove = []
                    upper_cut = np.percentile(y, 100-PARAMETERS.QUARTILE_CUT)
                    for i in range(len(x)):
                        if x[i] < PARAMETERS.SHOW_X_RANGE[0] or x[i] > PARAMETERS.SHOW_X_RANGE[1]:
                            toRemove.append(i)
                        if y[i] < PARAMETERS.SHOW_Y_RANGE[0] or y[i] > PARAMETERS.SHOW_Y_RANGE[1]:
                            toRemove.append(i)
                        if y[i] > upper_cut:
                            toRemove.append(i)

                    toRemove.sort(reverse=True)

                    for i in toRemove:
                        x.pop(i)
                        y.pop(i)

                    colors = ["blue"]*len(x)

                if PARAMETERS.SHOW_POINTS:
                    if PARAMETERS.REGRESSION_LINE:
                        if type == "norms":
                            nans = []
                            for i in range(len(y)):
                                if np.isnan(y[i]):
                                    y[i] = 0
                                    nans.append(i)
                            w = [1./(float(len(y)-len(nans)))]*len(y)
                            for n in nans:
                                w[n] = 0
                            s = UnivariateSpline(x, y, s=PARAMETERS.REG_DIMENSION, w=w)
                            xs = np.linspace(min(x), max(x), 100)
                            ys = s(xs)
                            plt.plot(xs,ys)
                        else:
                            X = np.array(list(x))
                            Y = np.array(list(y))
                            gauss = GP(X, Y)
                            x_guess = np.linspace(min(X), max(X), 400)
                            y_pred = np.vectorize(gauss.predict)(x_guess)
                            x += list(x_guess)
                            y += list(y_pred[0])
                            colors += ["orange"]*len(x_guess)
                            """
                            X = np.array(list(x))
                            X.reshape(-1, 1)
                            Y = np.array(list(y))
                            Y.reshape(-1, 1)
                            kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e2))
                            gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=9)
    
                            # Fit to data using Maximum Likelihood Estimation of the parameters
                            gp.fit(X, Y)
    
                            # Make the prediction on the meshed x-axis (ask for MSE as well)
                            y_pred, sigma = gp.predict(X, return_std=True)
    
                            # Plot the function, the prediction and the 95% confidence interval based on
                            # the MSE
                            fig = plt.figure()
                            plt.plot(X, Y, 'r.', markersize=10, label=u'Observations')
                            plt.plot(X, y_pred, 'b-', label=u'Prediction')
                            plt.fill(np.concatenate([X, x[::-1]]),
                                     np.concatenate([y_pred - 1.9600 * sigma,
                                                    (y_pred + 1.9600 * sigma)[::-1]]),
                                     alpha=.5, fc='b', ec='None', label='95% confidence interval')
                            plt.xlabel('$ratio$')
                            plt.ylabel('$time$')
                            plt.ylim(-10, 20)"""
                            #print y
                            plt.scatter(x, y, c= colors)
                            #plt.scatter(x_guess, y_pred[0], c ="orange")
                            #plt.plot(x_guess, y_pred[0], c="b")
                            #plt.plot(x_guess, y_pred[0] - np.sqrt(y_pred[1]) * 3, "r:")
                            #plt.plot(x_guess, y_pred[0] + np.sqrt(y_pred[1]) * 3, "r:")
                    else:
                        plt.scatter(x,y, label = "Budget "+str(b))

            plt.legend(loc='upper right')
            """plotfilename = self.plotfile[:len(self.plotfile)-4]+'_2D_SCATTER_'+type.upper()+"_"+str(repow)+'_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+str(budget)+'B'+str(gen)+'G.png'
            plt.suptitle(type.capitalize()+', '+str(repow)+', '+self.num_norms+'N, '+self.num_goods+'G\n'' '+str(PARAMETERS.ESTIMATOR)+' Budget = '+str(budget)+' GenProb = '+str(gen))
            if PARAMETERS.SAVE:
                plt.savefig(plotfilename)"""
            if PARAMETERS.SHOW:
                plt.show()
            plt.close(fig)

    def showDataFromPath(self, path1, path2):
        f1 = open(path1, "r")
        f2 = open(path2, "r")
        data1b1 = []
        data1b2 = []
        data1b3 = []
        data2b1 = []
        data2b2 = []
        data2b3 = []
        for l in f1:
            d = l.split(",")
            if d[1] == "1":
                data1b1.append(float(d[6]))
            elif d[1] == "2":
                data1b2.append(float(d[6]))
            elif d[1] == "3":
                data1b3.append(float(d[6]))
            else:
                print "ERROR"

        for l in f2:
            d = l.split(",")
            if d[1] == "1":
                data2b1.append(float(d[6]))
            elif d[1] == "2":
                data2b2.append(float(d[6]))
            elif d[1] == "3":
                data2b3.append(float(d[6]))
            else:
                print "ERROR"

        print "------MEANS------"
        mean1 = np.mean(data1b1)
        mean2 = np.mean(data2b1)
        average1 = mean1
        average2 = mean2
        print "Data1B1 Mean: "+str(mean1)+" Data2B1 Mean: "+str(mean2)+" Difference: "+str(mean1-mean2)
        mean1 = np.mean(data1b2)
        mean2 = np.mean(data2b2)
        average1 += mean1
        average2 += mean2
        print "Data1B2 Mean: "+str(mean1)+" Data2B2 Mean: "+str(mean2)+" Difference: "+str(mean1-mean2)
        mean1 = np.mean(data1b3)
        mean2 = np.mean(data2b3)
        average1 += mean1
        average2 += mean2
        print "Data1B3 Mean: "+str(mean1)+" Data2B3 Mean: "+str(mean2)+" Difference: "+str(mean1-mean2)
        print "Mean data 1:  "+str(average1/3)+" Mean data 2:  "+str(average2/3)+" Difference: "+str(average1/3-average2/3)

        print "------MEDIANS------"
        median1 = np.median(data1b1)
        median2 = np.median(data2b1)
        average1 = median1
        average2 = median2
        print "Data1B1 Median: "+str(median1)+" Data2B1 Median: "+str(median2)+" Difference: "+str(median1-median2)
        median1 = np.median(data1b2)
        median2 = np.median(data2b2)
        average1 += median1
        average2 += median2
        print "Data1B2 Median: "+str(median1)+" Data2B2 Median: "+str(median2)+" Difference: "+str(median1-median2)
        median1 = np.median(data1b2)
        median2 = np.median(data2b2)
        average1 += median1
        average2 += median2
        print "Data1B3 Median: "+str(median1)+" Data2B3 Median: "+str(median2)+" Difference: "+str(median1-median2)
        print "Mean data 1:  "+str(average1/3)+" Mean data 2:  "+str(average2/3)+" Difference: "+str(average1/3-average2/3)


        print "------STANDARD DEVIATION------"
        sd1 = np.std(data1b1)
        sd2 = np.std(data2b1)
        print "Data1B1 Median: " +str(sd1)+ " Data2B1 Median: "+str(sd2)+" Difference: "+str(abs(sd1-sd2))
        sd1 = np.std(data1b2)
        sd2 = np.std(data2b2)
        print "Data1B2 Median: " +str(sd1)+ " Data2B2 Median: "+str(sd2)+" Difference: "+str(abs(sd1-sd2))
        sd1 = np.std(data1b3)
        sd2 = np.std(data2b3)
        print "Data1B3 Median: " +str(sd1)+ " Data2B3 Median: "+str(sd2)+" Difference: "+str(abs(sd1-sd2))




    def plot3d(self):
        if PARAMETERS.DISTRIBUTE_NUM_NORMS:
            norms_or_relations = 'NORMS'
        else:
            norms_or_relations = 'RELS'
        self.loadData3d()
        gen_meansG = ((self.estimator(self.G11),self.estimator(self.G12),self.estimator(self.G13)),(self.estimator(self.G21),self.estimator(self.G22),self.estimator(self.G23)),(self.estimator(self.G31),self.estimator(self.G32),self.estimator(self.G33)))
        gen_stdG = ((np.std(self.G11),np.std(self.G12),np.std(self.G13)),(np.std(self.G21),np.std(self.G22),np.std(self.G23)),(np.std(self.G31),np.std(self.G32),np.std(self.G33)))
        gen_meansI = ((self.estimator(self.I11),self.estimator(self.I12),self.estimator(self.I13)),(self.estimator(self.I21),self.estimator(self.I22),self.estimator(self.I23)),(self.estimator(self.I31),self.estimator(self.I32),self.estimator(self.I33)))
        gen_stdI = ((np.std(self.I11),np.std(self.I12),np.std(self.I13)),(np.std(self.I21),np.std(self.I22),np.std(self.I23)),(np.std(self.I31),np.std(self.I32),np.std(self.I33)))

        fig = plt.figure()
        fig.suptitle(PARAMETERS.ESTIMATOR.capitalize()+' time of resolution, G, '+self.num_norms+'N, '+self.num_goods+'G')
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('Few', 'Medium', 'Lots'))
            if PARAMETERS.DISTRIBUTE_NUM_NORMS:
                ax.set_xlabel('Norms in the solution', labelpad = 20)
            else:
                ax.set_xlabel('Relations in the tree', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('Low', 'Medium', 'High'))
            ax.set_ylabel('Budget', labelpad = 20)
            ys = gen_meansG[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Time', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'G_3D_'+norms_or_relations+'_'+PARAMETERS.ESTIMATOR+'_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+'.png'
        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()

        fig = plt.figure()
        fig.suptitle('Standard deviation in the execution time, G, '+self.num_norms+'N, '+self.num_goods+'G')
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('Few', 'Medium', 'Lots'))
            if PARAMETERS.DISTRIBUTE_NUM_NORMS:
                ax.set_xlabel('Norms in the solution', labelpad = 20)
            else:
                ax.set_xlabel('Relations in the tree', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('Low', 'Medium', 'High'))
            ax.set_ylabel('Budget', labelpad = 20)
            ys = gen_stdG[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Time', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'G_3D_'+norms_or_relations+'_STD'+str(PARAMETERS.QUARTILE_CUT)+'Q'+'.png'
        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()

        fig = plt.figure()
        fig.suptitle(PARAMETERS.ESTIMATOR.capitalize()+' time of resolution, I, '+self.num_norms+'N, '+self.num_goods+'G')
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('Few', 'Medium', 'Lots'))
            if PARAMETERS.DISTRIBUTE_NUM_NORMS:
                ax.set_xlabel('Norms in the solution', labelpad = 20)
            else:
                ax.set_xlabel('Relations in the tree', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('Low', 'Medium', 'High'))
            ax.set_ylabel('Budget', labelpad = 20)
            ys = gen_meansI[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Time', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'I_3D_'+norms_or_relations+'_'+PARAMETERS.ESTIMATOR+'_'+str(PARAMETERS.QUARTILE_CUT)+'Q'+'.png'
        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()

        fig = plt.figure()
        fig.suptitle('Standard deviation in the execution time, I, '+self.num_norms+'N, '+self.num_goods+'G')
        ax = fig.add_subplot(111, projection='3d')
        for c, z in zip(['b', 'g', 'r'], [2, 1, 0]):
            xs = np.arange(3)
            ax.set_xticks(xs)
            ax.set_xticklabels(('Few', 'Medium', 'Lots'))
            if PARAMETERS.DISTRIBUTE_NUM_NORMS:
                ax.set_xlabel('Norms in the solution', labelpad = 20)
            else:
                ax.set_xlabel('Relations in the tree', labelpad = 20)
            ax.set_yticks(xs)
            ax.set_yticklabels(('Low', 'Medium', 'High'))
            ax.set_ylabel('Budget', labelpad = 20)
            ys = gen_stdI[z]
            cs = [c] * len(xs)
            ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

        ax.set_zlabel('Time', labelpad = 10)

        figpath = self.plotfile[:len(self.plotfile)-4]+'I_3D_'+norms_or_relations+'_STD'+str(PARAMETERS.QUARTILE_CUT)+'Q'+'.png'

        if PARAMETERS.SAVE:
            plt.savefig(figpath)
        if PARAMETERS.SHOW:
            plt.show()


    @staticmethod
    def interQuartile(set, erase = False):
        if set:
            returned_set = set
            removed = []
            if PARAMETERS.QUARTILE_CUT:
                upper_cut = np.percentile(set, 100-PARAMETERS.QUARTILE_CUT)
                lower_cut = np.percentile(set, PARAMETERS.QUARTILE_CUT)
                returned_set = []
                for x in set:
                    if x<= upper_cut and x>= lower_cut:
                        returned_set.append(x)
                    else:
                        removed.append(x)
            if erase:
                for x in removed:
                    set.remove(x)
        else:
            set.append(0)
            returned_set = [0]
        return returned_set

    @staticmethod
    def enrichPlotData(num_norms = None, num_goods = None):
        last_r = None
        last_b = None
        last_h = None
        last_w = None
        last_g = None
        num = 0
        num_roots = None
        if not num_norms:
            num_norms = PARAMETERS.NUM_NORMS_ENRICH
        if not num_goods:
            num_goods = PARAMETERS.NUM_GOODS_ENRICH
        currentPath = os.getcwd()
        plot_file_name = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+'G/plot'+str(num_norms)+'N'+str(num_goods)+'G.txt'
        plot_file = open(plot_file_name, 'r')
        lines = plot_file.readlines()
        plot_file.close()
        if len(lines[0].split(',')) == 7:
            plot_file = open(plot_file_name, 'w')
            for line in lines:
                data = line.split(',')
                repower = data[0]
                budget = float(data[1])/4
                h = data[2]
                w = data[3]
                g = float(data[4])
                if last_r == repower and last_b == budget and last_h == h and last_w == w and last_g ==g:
                    num += 1
                else:
                    num = 0
                lp_file_name = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+'G/LP/LP'+repower+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*100))+'b_'+str(num)+'.lp'
                res_file_name = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+'G/RESULTS/RESULT'+repower+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*100))+'b_'+str(num)+'.txt'
                lp_file = open(lp_file_name, 'r')
                res_file = open(res_file_name, 'r')
                num_rel = 0
                look = False
                for lp_line in lp_file:
                    if lp_line[:12] == "/*  # Roots:":
                        num_roots = int(lp_line[12:].strip('\n').replace('*/', ''))
                    if lp_line == "Binaries\n":
                        look = False
                    if look:
                        data = lp_line.strip('\n')
                        data = data[len(data)-1]
                        if data == '1':
                            num_rel += 1
                    if lp_line == "Subject To\n":
                        look = True
                look = False
                sol_norms = 0
                for res_line in res_file:
                    if res_line == " </variables>\n":
                        look = False
                    if look:
                        data = res_line.strip('\n')
                        data = data[2:].replace('<', '').replace('>', '').replace('/', '').replace('\"', '')
                        data = data.split(' ')
                        if data[3][6] == '1':
                            sol_norms += 1
                    if res_line == " <variables>\n":
                        look = True
                if num_roots:
                    num_gen_rel = 0
                    for i in range(1,int(h)+1):
                        num_gen_rel += i*num_roots*g*int(w)**i
                    num_ex_rel = num_rel-num_gen_rel
                    plot_file.write(line.strip('\n')+','+str(sol_norms)+','+str(num_ex_rel)+','+str(num_gen_rel)+'\n')
                last_r = repower
                last_b = budget
                last_h = h
                last_w = w
                last_g = g

    @staticmethod
    def eraseNaNData(set):
        return_set = []
        for i in range(len(set)):
            return_set.append([])
            for j in range(len(set[i])):
                if np.math.isnan(set[i][j]):
                    return_set[i].append(0)
                else:
                    return_set[i].append(set[i][j])
        return return_set

    def autolabel(self, rects, ax ):
        """
        Attach a text label above each bar displaying its height
        """
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                    '%d' % int(height),
                    ha='center', va='bottom')

    @staticmethod
    def sortData(x, y):
        for passnum in range(len(x)-1,0,-1):
            for i in range(passnum):
                if x[i]>x[i+1]:
                    temp = x[i]
                    x[i] = x[i+1]
                    x[i+1] = temp
                    temp = y[i]
                    y[i] = y[i+1]
                    y[i+1] = temp
