import os
import PARAMETERS

def main():
    file_name = os.getcwd()+'/'+str(PARAMETERS.NUM_NORMS_ORDER)+'N'+str(PARAMETERS.NUM_GOODS_ORDER)+'G'+'/plot'+str(PARAMETERS.NUM_NORMS_ORDER)+'.txt'
    file = open(file_name, 'r')
    ordered_lines = []
    for line in file:
        line = line.strip('\n')
        line = line.split(',')
        budget = ['low', 'medium', 'high'][int(line[1])-1]
        ordered_lines.append((float(line[6]), line[0], budget, int(line[2]), int(line[3]), float(line[4]), int(line[8]), int(line[7])))
    ordered_lines.sort(reverse=True)
    print "%11s%10s%10s%10s%10s%10s%10s%15s" % ('Time', 'Repower', 'Budget', 'Height', 'Width', 'Gen_prob', 'Ex. Rel.','Norms in sol.')
    for line in ordered_lines:
        print "%11.2f%10s%10s%10d%10d%10.2f%10d%15d" % line

if __name__ == '__main__':
    main()
