import os
import sys
import subprocess
sys.path.append(os.getcwd().replace('/Graphics', ''))
import PARAMETERS


def bidCalculator(norms, g, h, w):
    """
    Calculates the number of roots to produce the demanded number of norms with the problem's properties
    :param norms: The desired number of norms
    :param g: The problem's generalisation probability
    :param h: The problem's height
    :param w: The problem's width
    :return: The number of bids (roots) for the CATS file
    """
    x = 0
    for i in range(h+1):
        x += pow(w, i)
    x -= 1
    x *= g
    x += 1
    bids = int(round(norms/x))
    return bids


def catsFileGenerator(norms, goods, heights, widths, gen_prob, vmnsplb = PARAMETERS.CATS_FOR_VMNSPLB, execute = PARAMETERS.EXECUTE_SCRIPT, rounds = PARAMETERS.CATS_ROUNDS, start_round = 0):
    """
    Generates the script to generate the CATS file for a given problem specification
    :param num_norms: The number of norms in the problem
    :param num_goods: The number of goods in the problem
    :param heights: The heights of the CATS files
    :param widths: The widths of the CATS files
    :param gen_prob: The generalisation probabilities of the CATS files
    """
    currentPath = os.getcwd()
    if vmnsplb:
        gorv = "V"
    else:
        gorv = "G"
    scriptgen = currentPath+'/'+str(norms)+'N'+str(goods)+gorv+'/'
    if execute:
        scriptgen += 'CATS/generateCATS'+str(norms)+'.sh'
    else:
        scriptgen += 'generateCATS'+str(norms)+'.txt'
    f1 = open(scriptgen, 'w+')
    if execute:
        f1.write('#!/usr/bin/env bash\n')
    for h in heights:
        for w in widths:
            for g in gen_prob:
                for round in range(start_round, rounds):
                    bids = bidCalculator(norms, g, h, w)
                    CATS_EX_file = 'CATS_'+str(norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'_'+str(round)+'.txt'

                    command = ""

                    if vmnsplb:
                        CATS_VAL_file = 'CATS_VAL_' + str(norms) + 'N_' + str(h) + '_' + str(w) + '_' + str(int(g * 100)) + '_' + str(round) + '.txt'

                        if PARAMETERS.CATS_VALUE_DENSITY_ALPHA:
                            command = 'cats -d L4 -alpha '+str(PARAMETERS.CATS_VALUE_DENSITY_ALPHA) +' -filename ' + CATS_VAL_file + ' -goods ' + str(goods) + ' -bids ' + str(bids)
                        else:
                            command = 'cats -d arbitrary -filename ' + CATS_VAL_file + ' -goods ' + str(goods) + ' -bids ' + str(bids)
                        command += ';mv ' + CATS_VAL_file + "0000 " + CATS_VAL_file+";"

                    command += 'cats -d arbitrary -filename '+CATS_EX_file+' -goods '+str(goods)+' -bids '+str(bids)
                    command += ';mv '+CATS_EX_file+"0000 "+CATS_EX_file
                    f1.write(command+";")
    f1.close()
    if execute:

        print "Building CATS Files"
        if PARAMETERS.OS == "MAC":
            os.chmod(scriptgen, 0o777)
            sp = subprocess.Popen(scriptgen, cwd= currentPath+'/'+str(norms)+'N'+str(goods)+gorv+'/CATS')
            sp.wait()
        if PARAMETERS.OS == "LINUX":
            os.chmod(scriptgen, 0o777)
            f = open(scriptgen, 'r')
            for command in f.readlines()[1].split(';'):
                if command[:4] == "cats":
                    command = PARAMETERS.CATS_EXE_DIR+command.strip("\n")
                command = command.split(" ")
                if command[0] in [PARAMETERS.CATS_EXE_DIR+"cats", "mv"]:
                    sp = subprocess.Popen(command, cwd= currentPath+'/'+str(norms)+'N'+str(goods)+gorv+'/CATS')
                    sp.wait()
        print "Done"
        os.remove(scriptgen)

def main():

    if len(PARAMETERS.CATS_NUM_NORMS_LIST) == len(PARAMETERS.CATS_NUM_GOODS_LIST):
        for i in range(len(PARAMETERS.CATS_NUM_NORMS_LIST)):
            norms = PARAMETERS.CATS_NUM_NORMS_LIST[i]
            goods = PARAMETERS.CATS_NUM_GOODS_LIST[i]
            catsFileGenerator(norms, goods, PARAMETERS.CATS_HEIGHT_LIST, PARAMETERS.CATS_WIDTH_LIST, PARAMETERS.CATS_GEN_PROBS_LIST)
    else:
        print "Incorrect input: CATS_NUM_NORMS_LIST has not the same ammount of elements as CATS_NUM_GOODS_LIST"

if __name__ == "__main__":
    main()
