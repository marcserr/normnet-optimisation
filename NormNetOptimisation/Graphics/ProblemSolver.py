from use import problem
import os
import sys
import time
import subprocess
import networkx as nx
import matplotlib.pyplot as plt
import random
import math
import ProjectExceptions
sys.path.append(os.getcwd().replace('Graphics', 'Solvers')+'/Cats_to_LP')
sys.path.append(os.getcwd().replace('/Graphics', ''))
import PARAMETERS
from CATSAuction import CATSAuction
import CATSFileUtilities


class ProblemSolver:
    """
    This class builds and solves the problems
    """

    def __init__(self):
        self.cases_to_build = PARAMETERS.CASES_TO_BUILD
        self.heights_build = PARAMETERS.HEIGHT_LIST_BUILD
        self.widths_build = PARAMETERS.WIDTH_LIST_BUILD
        self.gen_probs_build = PARAMETERS.GEN_PROBS_LIST_BUILD
        if isinstance(PARAMETERS.REPOWERS_BUILD, str):
            self.repowers_build = [PARAMETERS.REPOWERS_BUILD]
        else:
            self.repowers_build = PARAMETERS.REPOWERS_BUILD
        self.cases_to_solve = PARAMETERS.CASES_TO_SOLVE
        self.heights_solve = PARAMETERS.HEIGHT_LIST_SOLVE
        self.widths_solve = PARAMETERS.WIDTH_LIST_SOLVE
        self.gen_probs_solve = PARAMETERS.GEN_PROBS_LIST_SOLVE
        self.repower_solve = PARAMETERS.REPOWER_SOLVE
        self.time_allowed_solving = PARAMETERS.MAX_TIME_ALLOWED_SOLVING
        self.budgets_to_build = PARAMETERS.BUDGETS_TO_BUILD
        self.budgets_to_solve = PARAMETERS.BUDGETS_TO_SOLVE

    def buildLP(self, lp_files, problem_type, p_generalization, height, width, repowers, budget, build_round, cats_ex_file, cats_val_file = None, sigma_g=0, sigma_h=0, sigma_w=0, preferences = None, vals_file = None):
        """
        Loads the CATS auction and builds the LP file
        :param cats_file: The cats file from which we build the LP file
        :param lp_files: The paths to the LPs that have to be created
        :param problem_type: The type of problem to code into LP files (MNSLP, MNSPLB,...)
        :param p_generalization: The probability of which a root in the graph will have a tree be
        :param height: The height of the problem graph
        :param width: The width of the problem graph
        :param repowers: The representation power functions of the LP problems
        :param budget: The budgets of the LP problems
        :param sigma_g: The variability of the generalisation probability
        :param sigma_h: The variability of the height
        :param sigma_w: The variability of the width
        """
        auction = CATSAuction()
        auction.load_file(cats_ex_file, cats_val_file)
        formula, num_exc_rels = auction.to_LP(False,False, vals_file)
        formula.write_LP(problem_type, repowers, p_generalization, sigma_g, height, sigma_h, width, sigma_w, budget, build_round, num_exc_rels, file_paths = lp_files, preferences = preferences)

    def casesToBuild(self, num_norms, num_goods, gorv):
        """
        This method check wich cases remain to be built and saves them into a file
        :param num_norms: The problems with this number of norms will be checked
        :param num_goods: The problems with this number of goods will be checked
        :return: True if there are cases left, False otherwise
        """
        casesLeft = False
        currentPath = os.getcwd()
        casesFile = open(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToBuild.txt', 'w')
        for budget in self.budgets_to_build:
            for h in self.heights_build:
                for w in self.widths_build:
                    for g in self.gen_probs_build:
                        writeCase = False
                        for repower in self.repowers_build:
                            lp_file_name = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/LP'+repower+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*25))+'b.lp'
                            if os.path.exists(lp_file_name):
                                #Check that the number of norms is within the tolerance of 5%, if not redo the experiment
                                lp_file = open(lp_file_name, 'r')
                                try:
                                    norms = self.getNumNorms(lp_file)
                                    if norms < num_norms*(1-PARAMETERS.NUM_NORMS_TOLERANCE_BUILD) or norms > num_norms*(1+PARAMETERS.NUM_NORMS_TOLERANCE_BUILD):
                                        casesLeft = True
                                        writeCase = True
                                    lp_file.close()
                                except:
                                    os.remove(lp_file_name)
                                    structFile = lp_file_name.replace("LPS", "STRUCT").replace("LPI", "STRUCT").replace(".lp", ".txt").replace("LP", "STRUCT")
                                    if os.path.exists(structFile):
                                        os.remove(structFile)
                                    casesLeft = True
                                    writeCase = True
                            else:
                                #Include the task in the tasks to do
                                casesLeft = True
                                writeCase = True

                        if writeCase:
                            casesFile.write(str(int(budget))+','+str(h)+','+str(w)+','+str(int(g*100))+'\n')

        casesFile.close()
        return casesLeft

    def casesToSolve(self, num_norms, num_goods, solve_round, gorv):
        """
        This method checks which cases remain to be solved and saves them into a file
        :param num_norms: The problems to solve with this number of norms will be checked
        :param num_goods: The problems to solve with this number of goods will be checked
        :return: True if there are cases left to solve, False otherwise
        """
        casesLeft = False
        currentPath = os.getcwd()
        casesFile = open(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToSolve.txt', 'w')
        for budget in self.budgets_to_solve:
            for h in self.heights_solve:
                for w in self.widths_solve:
                    for g in self.gen_probs_solve:
                        for repower in self.repower_solve:
                            plot_file_name = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/plot'+str(num_norms)+'N'+str(num_goods)+gorv+'_'+str(solve_round)+'.txt'
                            if os.path.exists(plot_file_name):
                                #Check that the number of norms is within the tolerance of 5%, if not redo the experiment
                                if not self.checkCaseInPlotFile(plot_file_name, budget, h, w, g, repower) :
                                    casesLeft = True
                                    casesFile.write(str(int(budget))+','+str(h)+','+str(w)+','+str(int(g*100))+','+str(repower)+'\n')
                            else:
                                #Include the task in the tasks to do
                                casesLeft = True
                                casesFile.write(str(int(budget))+','+str(h)+','+str(w)+','+str(int(g*100))+','+str(repower)+'\n')
        casesFile.close()
        return casesLeft

    def checkCaseInPlotFile(self, plot_file_name, budget, h, w, g, repower):
        """
        Checks if a particular case is in the plotfile
        :param plot_file_name: The plot file to check from
        :param budget: The budget of the case to chech
        :param h: The height of the case to check
        :param w: The width of the case to check
        :param g: The generalisation power of the case to check
        :param repower: The representation power function of the case to check
        :return: True if the case is in the plot file, False otherwise
        """
        found = False
        size = os.path.getsize(plot_file_name)
        plot_file = open(plot_file_name, 'r')
        case = [repower, str(int(budget)), str(h), str(w), str(g)]
        while not found and size:
            line = plot_file.readline()
            data = line.strip('\n').split(',')[:5]
            equal = True
            if not isinstance(data, list) or len(data) != 5:
                print 'ERROR: Line in plot file not satisfying the structure: '+str(data)
            else:
                for i in range(5):
                    if data[i] != case[i]:
                        equal = False
                if equal:
                    found = True
            size -= len(line)
        plot_file.close()
        return found

    def getNumNorms(self, lpfile):
        """
        Reads a LP file and returns the number of norms in the graph of it
        :param lpfile: The LP file to read
        :return: The number of norms in the LP's problem graph
        """
        i = 0
        found = None
        while not found:
            line = lpfile.readline()
            if i >= 8:
                found = line.split()[3]
            i += 1
        return int(found)

    def getCasesToBuildLeft(self, num_norms, num_goods, gorv):
        """
        Returns a list of the cases left to be built for a particular problem
        :param num_norms: The number of norms in the problem
        :param num_goods: The number of goods in the problem
        :return: The cases that remain to be built
        """
        casesLeft = []
        currentPath = os.getcwd()
        cases_file = open(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToBuild.txt', 'r')
        for line in cases_file:
            casesLeft.append(line[:len(line)-1].split(','))
        return casesLeft

    def getCasesToSolveLeft(self, num_norms, num_goods, gorv):
        """
        Returns a list of the cases left to be solved for a particular problem
        :param num_norms: The number of norms in the problem
        :param num_goods: The number of goods in the problem
        :return: The cases that remain to be solved
        """
        casesLeft = []
        currentPath = os.getcwd()
        file = open(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToSolve.txt', 'r')
        for line in file:
            casesLeft.append(line[:len(line)-1].split(','))
        return casesLeft

    def cleanPlotFile(self, num_norms, num_goods, solve_round, gorv):
        """
        Cleans the plot file from lines of experiments with more norms than permitted
        :param num_norms: The number of norms in the problem
        :param num_goods: The number of goods in the problem
        """
        currentPath = os.getcwd()
        file = open(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/plot'+str(num_norms)+'N'+str(num_goods)+gorv+'_'+str(solve_round)+'.txt', 'r')
        lines = file.readlines()
        file.close()
        file = open(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/plot'+str(num_norms)+'N'+str(num_goods)+gorv+'_'+str(solve_round)+'.txt', 'w')
        for line in lines:
            norms = int(line.split(',')[5])
            if norms >= num_norms*(1-PARAMETERS.NUM_NORMS_TOLERANCE_SOLVE) and norms <= num_norms*(1+PARAMETERS.NUM_NORMS_TOLERANCE_SOLVE):
                file.write(line)
        file.close()

    def build(self):
        """
        Builds the LP files, if the CATS file is missing or cannot be used it rebuilds it, if an LP
        has more norms than expected it discards it and rebuilds it. After all LP files are built
        it moves them to the correct location.
        """
        if PARAMETERS.BUILD_PROBLEM_TYPE == 'VMNSPLB':
            gorv = "V"
        else:
            gorv = "G"
        currentPath = os.getcwd()
        for case in self.cases_to_build:
            num_norms = case[0]
            num_goods = case[1]
            start = 0

            if PARAMETERS.DETECT_BUILD_START:
                files = os.listdir(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/LP')
                for f in files:
                    num = f.split('_')[-1].replace('.lp', '')
                    if num.isdigit():
                        start = max(start, int(num))
                if start != 0:
                    start += 1
            elif PARAMETERS.START_BUILD_ROUND:
                start = PARAMETERS.START_BUILD_ROUND

            if len(os.listdir(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/CATS')) == 0:
                if PARAMETERS.BUILD_PROBLEM_TYPE == 'VMNSPLB':
                    vmnsplb = True
                else:
                    vmnsplb = False
                CATSFileUtilities.catsFileGenerator(num_norms, num_goods, PARAMETERS.HEIGHT_LIST_BUILD, PARAMETERS.WIDTH_LIST_BUILD,PARAMETERS.GEN_PROBS_LIST_BUILD,vmnsplb,True, PARAMETERS.BUILD_ROUNDS, start)

            for build_round in range(start, PARAMETERS.BUILD_ROUNDS):
                if PARAMETERS.PROB_BAD_NORM or PARAMETERS.PROB_GREAT_NORM:
                    CATS_modified = False
                else:
                    CATS_modified = True
                areCasesLeft = self.casesToBuild(num_norms, num_goods, gorv)
                iter = 0
                while areCasesLeft and not iter >= PARAMETERS.MAX_BUILD_ITERATIONS:
                    casesLeft = self.getCasesToBuildLeft(num_norms, num_goods, gorv)
                    for case_left in casesLeft:
                        LPbuilt = False
                        budget = float(case_left[0])/4
                        h = int(case_left[1])
                        w = int(case_left[2])
                        g = float(case_left[3])/100
                        cats_val_file = None
                        preferences = None
                        vals_file = None
                        if PARAMETERS.BUILD_PROBLEM_TYPE == "VMNSPLB" and PARAMETERS.BUILD_PREFERENCES:
                            G = self.randomPreferenceGraph(num_goods)
                            preferencesFile = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/PREF/PREF_'+str(num_norms)+'N'+str(num_goods)+gorv+'_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*100))+'b_'+str(build_round)+'.txt'
                            preferences = self.buildPreferences(G, preferencesFile)
                            cats_val_file = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/CATS/CATS_VAL_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'_'+str(build_round)+'.txt'
                            vals_file = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/VALUES/VALUES_'+str(num_norms)+'N'+str(num_goods)+gorv+'_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*100))+'b_'+str(build_round)+'.txt'
                            if not os.path.exists(cats_val_file):
                                ProblemSolver.createCATS(num_norms, num_goods, h, w, g, build_round, gorv, True)
                            if not CATS_modified:
                                orderedPreferedValues, utilities = ProblemSolver.orderedValues(preferences)
                                ProblemSolver.modifyCATS(cats_val_file,orderedPreferedValues, utilities)
                                CATS_modified = True

                        while not LPbuilt:
                            print 'Building: b= '+str(int(budget*100))+' h= '+str(h)+' w= '+str(w)+" g= "+str(int(g*100))+" Round: "+str(build_round)
                            start_time = time.time()
                            cats_ex_file = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/CATS/CATS_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'_'+str(build_round)+'.txt'
                            if os.path.exists(cats_ex_file):
                                if PARAMETERS.BUILD_PROBLEM_TYPE == "VMNSPLB":
                                    ProblemSolver.trimLongestCATS(cats_ex_file, cats_val_file)
                                lp_files = []
                                for repower in self.repowers_build:
                                    lp_files.append(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/LP'+str(repower)+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*100))+'b.lp')
                                try:
                                    self.buildLP(lp_files, PARAMETERS.BUILD_PROBLEM_TYPE, g, h, w, self.repowers_build, budget, build_round, cats_ex_file, cats_val_file, preferences = preferences, vals_file = vals_file)
                                    duration = time.time()-start_time
                                    print 'Built in ' + str(duration) + ' s'
                                    LPbuilt = True
                                except IndexError:
                                    print '\033[91m'+'ERROR: UNABLE TO BUILD LP FOR PROBLEM: b= '+str(int(budget*100))+' h= '+str(h)+' w= '+str(w)+" g= "+str(int(g*100))+'\033[0m'
                                    for lp_file in lp_files:
                                        if os.path.exists(lp_file):
                                            os.remove(lp_file)
                                            structFile = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/STRUCT/'+lp_file.replace("LPS", "STRUCT").replace("LPI", "STRUCT").replace(".lp", "_"+str(build_round)+".txt").replace("_"+str(num_norms)+"N", "").split('/')[-1]
                                            if os.path.exists(structFile):
                                                os.remove(structFile)
                                        if PARAMETERS.REBUILD_CATS_IF_FAIL:
                                            ProblemSolver.createCATS(num_norms, num_goods, h, w, g, build_round, gorv)
                                            if PARAMETERS.BUILD_PROBLEM_TYPE == "VMNSPLB":
                                                ProblemSolver.createCATS(num_norms, num_goods, h, w, g, build_round, gorv, True)
                                                if PARAMETERS.PROB_BAD_NORM or PARAMETERS.PROB_GREAT_NORM:
                                                    cats_val_file = os.getcwd()+"/"+str(num_norms)+"N"+str(num_goods)+gorv+"/CATS/CATS_VAL_"+str(num_norms)+"N_"+str(h)+"_"+str(w)+"_"+str(int(g*100))+"_"+str(build_round)+".txt"
                                                    orderedPreferedValues, utilities = ProblemSolver.orderedValues(preferences)
                                                    ProblemSolver.modifyCATS(cats_val_file, orderedPreferedValues, utilities)
                            else:
                                print 'Unable to build, missing CATS file'
                                if PARAMETERS.REBUILD_CATS_IF_FAIL:
                                    ProblemSolver.createCATS(num_norms, num_goods, h, w, g, build_round, gorv)
                    iter += 1
                    areCasesLeft = self.casesToBuild(num_norms, num_goods, gorv)
                    if areCasesLeft:
                        casesLeft = self.getCasesToBuildLeft(num_norms, num_goods, gorv)
                        if iter < PARAMETERS.MAX_BUILD_ITERATIONS and PARAMETERS.REBUILD_CATS_IF_FAIL:
                            for case_left in casesLeft:
                                h = int(case_left[1])
                                w = int(case_left[2])
                                g = float(case_left[3])/100
                                ProblemSolver.createCATS(num_norms, num_goods, h, w, g, build_round, gorv)
                                if PARAMETERS.BUILD_PROBLEM_TYPE == "VMNSPLB":
                                    ProblemSolver.createCATS(num_norms, num_goods, h, w, g, build_round, gorv, True)
                                    if PARAMETERS.PROB_BAD_NORM or PARAMETERS.PROB_GREAT_NORM:
                                        cats_val_file = os.getcwd() + "/" + str(num_norms) + "N" + str(num_goods) + gorv + "/CATS/CATS_VAL_" + str(num_norms) + "N_" + str(h) + "_" + str(w) + "_" + str(int(g * 100)) + "_" + str(build_round) + ".txt"
                                        preferencesFile = currentPath + '/' + str(num_norms) + 'N' + str(num_goods) + gorv + '/PREF/PREF_' + str(num_norms) + 'N' + str(num_goods) + gorv + '_' + str(h) + '_' + str(w) + '_' + str(int(g * 100)) + 'g_' + str(int(budget * 100)) + 'b_' + str(build_round) + '.txt'
                                        preferences = ProblemSolver.readPreferences(preferencesFile)
                                        orderedPreferedValues, utilities = ProblemSolver.orderedValues(preferences)
                                        ProblemSolver.modifyCATS(cats_val_file, orderedPreferedValues, utilities)
                        elif iter >= PARAMETERS.MAX_BUILD_ITERATIONS:
                            print "---------MAXIMUM ITERATIONS REACHED---------"
                            for case_left in casesLeft:
                                budget = int(case_left[0])
                                h = int(case_left[1])
                                w = int(case_left[2])
                                g = int(case_left[3])
                                lp_file_name_S = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/LPS'+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(g)+'g_'+str(budget*25)+'b.lp'
                                lp_file_name_I = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/LPI'+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(g)+'g_'+str(budget*25)+'b.lp'
                                for pair in [(lp_file_name_S, 'S'), (lp_file_name_I, 'I')]:
                                    lp_file_name, repower = pair
                                    if os.path.exists(lp_file_name):
                                        lp_file = open(lp_file_name)
                                        norms = self.getNumNorms(lp_file)
                                        lp_file.close()
                                        os.remove(lp_file_name)
                                        structFile = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/STRUCT/'+lp_file_name.replace("LPS", "STRUCT").replace("LPI", "STRUCT").replace(".lp", "_"+str(build_round)+".txt").replace("_"+str(num_norms)+"N", "").split('/')[-1]
                                        if os.path.exists(structFile):
                                            os.remove(structFile)
                                        print 'Not useful LP file deleted: b= ' + str(budget * 25) + ' h= ' + str(h) + ' w= ' + str(w) + " g= " + str(g) + " Repower= " + repower + " Norms: " + str(norms)
                            ProblemSolver.moveLPFiles(num_norms, num_goods, build_round, gorv)

                if not areCasesLeft:
                    ProblemSolver.moveLPFiles(num_norms, num_goods, build_round, gorv)
                    print "---------ALL PROBLEMS BUILD SUCCESFULLY---------"
                    if os.path.exists(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToBuild.txt'):
                        os.remove(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToBuild.txt')
                elif os.path.exists(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToBuild.txt'):
                    os.rename(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToBuild.txt', currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesNotBuilt.txt')


    def solve(self):
        """
        Solves the LP files, and writes the outcome in the plot file
        """
        if PARAMETERS.SOLVE_PROBLEM_TYPE == 'VMNSPLB':
            gorv = 'V'
        else:
            gorv = 'G'

        currentPath = os.getcwd()
        for case in self.cases_to_solve:
            num_norms = case[0]
            num_goods = case[1]
            start = 0
            if PARAMETERS.DETECT_SOLVE_START:
                files = os.listdir(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/RESULTS')
                for f in files:
                    num = f.split('_')[-1].replace('.txt', '')
                    if num.isdigit():
                        start = max(start, int(num))
            elif PARAMETERS.START_SOLVE_ROUND:
                start = PARAMETERS.START_SOLVE_ROUND
            for solve_round in range(start, PARAMETERS.SOLVE_ROUNDS):
                iter = 0
                current_plot_file = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/plot'+str(num_norms)+'N'+str(num_goods)+gorv+'_'+str(solve_round)+'.txt'
                if os.path.exists(current_plot_file):
                    os.remove(current_plot_file)
                while self.casesToSolve(num_norms, num_goods, solve_round, gorv) and not iter >= PARAMETERS.MAX_SOLVE_ITERATIONS:
                    solve_cases = self.getCasesToSolveLeft(num_norms, num_goods, gorv)
                    for solve_case in solve_cases:
                        budget = float(solve_case[0])/4
                        h = int(solve_case[1])
                        w = int(solve_case[2])
                        g = float(solve_case[3])/100
                        repower = solve_case[4]
                        print 'Solving: b= ' + str(int(budget * 4)) + ' h= ' + str(h) + ' w= ' + str(w) + " g= " + str(int(g * 100)) + " repower= " + repower + " round= " + str(solve_round)
                        lp_file = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/LP/LP'+repower+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*100))+'b_'+str(solve_round)+'.lp'
                        if os.path.exists(lp_file):
                            result_file = currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/RESULTS/RESULT'+repower+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*100))+'b_'+str(solve_round)+'.txt'
                            problem(repower, int(budget*4), lp_file, result_file, self.time_allowed_solving, h, w, g, plotfile = current_plot_file)
                        else:
                            print 'Unable to Solve, missing LP file'
                    self.cleanPlotFile(num_norms, num_goods, solve_round, gorv)
                    iter += 1
                if iter >= PARAMETERS.MAX_SOLVE_ITERATIONS and self.casesToSolve(num_norms, num_goods, solve_round, gorv):
                    print "---------MAXIMUM ITERATIONS REACHED (round: "+str(solve_round)+")---------"
                    plot_data_file = currentPath.replace('Graphics', '')+'PLOTDATA/'+str(num_norms)+'N'+str(num_goods)+gorv+'/plot'+str(num_norms)+'N'+str(num_goods)+gorv+'.txt'
                    if not os.path.exists(plot_data_file):
                        pf = open(plot_data_file, "w")
                        pf.close()
                    if os.path.exists(current_plot_file):
                        outfile = open(plot_data_file, "a")
                        infile = open(current_plot_file, "r")
                        for line in infile:
                            if line != '\n':
                                outfile.write(line)
                        outfile.close()
                        infile.close()
                        os.remove(current_plot_file)
                else:
                    print "---------ALL PROBLEMS SOLVED SUCCESFULLY (round: "+str(solve_round)+")---------"
                    plot_data_file = currentPath.replace('Graphics', '')+'PLOTDATA/'+str(num_norms)+'N'+str(num_goods)+gorv+'/plot'+str(num_norms)+'N'+str(num_goods)+gorv+'.txt'
                    if not os.path.exists(plot_data_file):
                        f = open(plot_data_file, "w")
                        f.close()
                    if os.path.exists(current_plot_file):
                        outfile = open(plot_data_file, "a")
                        infile = open(current_plot_file, "r")
                        for line in infile:
                            if line != '\n':
                                outfile.write(line)
                        outfile.close()
                        infile.close()
                        os.remove(current_plot_file)
            if os.path.exists(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToSolve.txt'):
                os.remove(currentPath+'/'+str(num_norms)+'N'+str(num_goods)+gorv+'/casesToSolve.txt')
            else:
                print "---------ALL PROBLEMS ALREADY SOLVED---------"


    def randomPreferenceGraph(self, num_goods, type = PARAMETERS.VALUE_ORDER_GENERATION_TYPE):
        if type == "TOTAL":
            G = nx.DiGraph()
            for i in range(num_goods):
                G.add_node(i)
            nodes = G.nodes()
            random.shuffle(nodes)
            for i in range(1, len(nodes)):
                G.add_edge(nodes[i-1], nodes[i])
        else:
            if PARAMETERS.VALUE_EQUALITIES:
                types = ["unknown", "preferred", "not_preferred", "equality"]
            else:
                types = ["unknown", "preferred", "not_preferred"]
            G = nx.DiGraph()
            for i in range(num_goods):
                G.add_node(i)
            nodes = list(G.nodes())
            while nodes:
                currentNode = random.choice(nodes)
                nodes.remove(currentNode)
                for node in nodes:
                    relationType = random.choice(types)
                    if relationType == "preferred" and not nx.has_path(G, node, currentNode):
                        G.add_edge(currentNode, node)
                    elif relationType == "not_preferred" and not nx.has_path(G, currentNode, node):
                        G.add_edge(node, currentNode)
                    elif relationType == "equality" and not nx.has_path(G, currentNode, node) and not nx.has_path(G, node, currentNode):
                        G.add_edge(node, currentNode)
                        G.add_edge(currentNode, node)
        return G

    def equalPairs(self, G):
        pairs = []
        for n1 in G.nodes():
            for n2 in G.nodes():
                if G.has_edge(n1, n2) and G.has_edge(n2, n1) and not (n2, n1) in pairs:
                    pairs.append((n1,n2))
        return pairs

    def checkLoops(self, G, layers):
        loops = False
        for n in G.nodes():
            for n2 in G.nodes():
                if n != n2 and nx.has_path(G, n, n2) and nx.has_path(G, n2, n) and not (dict(G[n]).has_key(n2) or dict(G[n2]).has_key(n)):
                    show = True
                    for l in layers:
                        if n in l and n2 in l:
                            show =False
                    if show:
                        loops = True
                        nx.spring_layout(G)
                        nx.draw(G, with_labels= True)
                        plt.show()
                        print n2, n
        return loops

    def makeCongruent(self, G, layers):
        if self.checkLoops(G, layers):
            raise ProjectExceptions.IncongruentGraphException
        for l in layers:
            for n in l:
                for pred in G.predecessors(n):
                    for n2 in l:
                        if not G.has_edge(pred, n2) and not pred == n2:
                            if not nx.has_path(G, n2, pred) or pred in l:
                                G.add_edge(pred, n2)
                            else:
                                raise ProjectExceptions.IncongruentGraphException

                for succ in G.successors(n):
                    for n2 in l:
                        if not G.has_edge(n2, succ) and not succ == n2:
                            if not nx.has_path(G, succ, n2) or succ in l:
                                G.add_edge(n2, succ)
                            else:
                                raise ProjectExceptions.IncongruentGraphException

    def randomDirection(self, G, layers):
        for l in layers:
            for i in range(len(l)):
                n1 = l[i]
                for j in range(len(l)):
                    n2 = l[j]
                    if n1 != n2:
                        G.remove_edge(n1, n2)
            random_arrangement = list(l)
            random.shuffle(random_arrangement)
            for i in range(len(l)):
                node = random_arrangement[i]
                for j in range(i+1,len(l)):
                    node2 = random_arrangement[j]
                    G.add_edge(node, node2)

    def equalLayers(self, pairs):
        layers = list(pairs)
        layerUpdate = True
        while layerUpdate:
            layerUpdate = False
            i = 0
            while i < len(layers) and not layerUpdate:
                currentLayer = list(layers[i])
                j = 0
                while j < len(layers) and not layerUpdate:
                    otherLayer = list(layers[j])
                    if not i==j and list(set(currentLayer) & set(otherLayer)):
                        layerUpdate = True
                    else:
                        j += 1
                if not layerUpdate:
                    i += 1
            if layerUpdate:
                l1 = layers.pop(max(i,j))
                l2 = layers.pop(min(i,j))
                layers.append(list(set().union(l1, l2)))
        return layers



    def buildPreferences(self, G, filename = None):
        if PARAMETERS.VALUE_EQUALITIES:
            pairs = self.equalPairs(G)
            equalLayers = self.equalLayers(pairs)
            try:
                self.makeCongruent(G, equalLayers)
            except ProjectExceptions.IncongruentGraphException:
                print "Incongurent Graph"
            self.randomDirection(G, equalLayers)
            if self.checkLoops(G, []):
                raise ProjectExceptions.IncongruentGraphException
        toDo = list(G.nodes())
        preferences = {}
        for edge in G.edges():
            if edge[0] in toDo:
                toDo.remove(edge[0])
        while toDo:
            node = toDo.pop(0)
            repeat = False
            pref = random.uniform(0, 1)
            i = 0
            successors = list(G.successors(node))
            while i < len(successors) and not repeat:
                succ = successors[i]
                if preferences.has_key(succ):
                    pref += preferences[succ]
                    i += 1
                else:
                    repeat = True
            if repeat:
                toDo.append(node)
            else:
                preferences[node] = pref
                for pred in G.predecessors(node):
                    if pred not in toDo:
                        toDo.append(pred)
        if filename:
            f = open(filename, "w")
            f.write("Nodes:\n")
            for node in G.nodes():
                f.write(str(node)+"\n")
            f.write("Edges:\n")
            for edge in G.edges():
                f.write(str(edge)+"\n")
            f.write("Utility values:\n")
            for node in preferences.keys():
                f.write(str(node)+" : "+str(preferences[node])+"\n")

        return preferences

    @staticmethod
    def moveLPFiles(num_norms, num_goods, build_round, gorv):
        """
        Moves all the LPs of a type to the correct directory
        :param num_norms: The number of norms of the LPs
        :param num_goods: The number of goods of the LPs
        :param build_round: The building round of the LPs
        """
        currentPath = os.getcwd()+'/'+str(num_norms)+'N'+str(num_goods)+gorv
        for budget in PARAMETERS.BUDGETS_TO_BUILD:
            for h in PARAMETERS.HEIGHT_LIST_BUILD:
                for w in PARAMETERS.WIDTH_LIST_BUILD:
                    for g in PARAMETERS.GEN_PROBS_LIST_BUILD:
                        for repower in PARAMETERS.REPOWERS_BUILD:
                            lp_file = currentPath+'/LP'+repower+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*25))+'b.lp'
                            if os.path.exists(lp_file):
                                new_lp_file = currentPath+'/LP/LP'+repower+'_'+str(num_norms)+'N_'+str(h)+'_'+str(w)+'_'+str(int(g*100))+'g_'+str(int(budget*25))+'b_'+str(build_round)+'.lp'
                                os.rename(lp_file, new_lp_file)

    @staticmethod
    def createCATS(num_norms, num_goods, h, w, g, build_round, gorv, values = False):
        """
        Creates a particular CATS file
        :param num_norms: The number of norms of the CATS file
        :param num_goods: The number of goods of the CATS file
        :param h: The height of the problem
        :param w: The width of the problem
        :param g: The generalisation probability of the problem
        :param build_round: The building round of the problem
        """
        if values:
            val ="VAL_"
        else:
            val = ""
        cats_directory = os.getcwd()+"/"+str(num_norms)+"N"+str(num_goods)+gorv+"/CATS"
        cats_filename = "CATS_"+val+str(num_norms)+"N_"+str(h)+"_"+str(w)+"_"+str(int(g*100))+"_"+str(build_round)+".txt"
        cats_goods = num_goods
        cats_bids = CATSFileUtilities.bidCalculator(num_norms, g, h, w)

        cats_command = [PARAMETERS.CATS_EXE_DIR+"cats", "-d", "arbitrary", "-filename", str(cats_filename), "-goods", str(cats_goods), "-bids", str(cats_bids)]
        if PARAMETERS.BUILD_PROBLEM_TYPE == "VMNSPLB" and PARAMETERS.BUILD_CATS_VALUE_DENSITY and values:
            cats_command[2] = "L4"
            cats_command.insert(3, "-alpha")
            cats_command.insert(4, str(PARAMETERS.BUILD_CATS_VALUE_DENSITY))
        if os.path.exists(cats_filename):
            remove_command = ["rm", cats_filename]
            removePrevious = subprocess.Popen(remove_command, cwd = cats_directory)
            removePrevious.wait()
        create_CATS = subprocess.Popen(cats_command, cwd= cats_directory)
        create_CATS.wait()
        rename_command = ["mv", cats_filename+"0000", cats_filename]
        rename_CATS = subprocess.Popen(rename_command, cwd= cats_directory)
        rename_CATS.wait()

    @staticmethod
    def modifyCATS(filename, valuePreferences, utilities):
        f = open(filename, "r")
        modified = False
        lines = f.readlines()
        start = 0
        i = 0
        while i < len(lines) and not modified:
            lines[i] = lines[i].strip("\n")
            if lines[0][:9] == "%Modified":
                modified = True
            else:
                stop = False
                if (lines[i] == "" or lines[i][0] == '%' or lines[i][:5] == 'goods' or lines[i][:4] == 'bids' or lines[i][:5] == 'dummy') and not stop:
                    start +=1
                else:
                    stop = True
            i += 1
        f.close()
        if not modified:
            f = open(filename, "w")
            f.write("%Modified:"+lines[0][1:]+"\n")
            for i in range(1, start):
                f.write(lines[i]+"\n")

            for i in range(start, len(lines)):
                if PARAMETERS.NORM_DUALITY and PARAMETERS.PROB_GREAT_NORM+PARAMETERS.PROB_BAD_NORM < 1+1e-10 and PARAMETERS.PROB_GREAT_NORM+PARAMETERS.PROB_BAD_NORM > 1-1e-10:
                    good = random.random() < PARAMETERS.PROB_GREAT_NORM
                    bad = not good
                elif PARAMETERS.NORM_DUALITY:
                    print "BADLY DEFINED PROBABILITIES"
                    raise ProjectExceptions.IncongruentProbabilitiesException
                else:
                    good = random.random() < PARAMETERS.PROB_GREAT_NORM
                    bad = random.random() < PARAMETERS.PROB_BAD_NORM
                    while (good and bad):
                        good = random.random() < PARAMETERS.PROB_GREAT_NORM
                        bad = random.random() < PARAMETERS.PROB_BAD_NORM

                if good:
                    newline = ProblemSolver.goodline(lines[i], valuePreferences, utilities)+"\n"
                elif bad:
                    newline = ProblemSolver.badline(lines[i], valuePreferences, utilities)+"\n"
                else:
                    newline = lines[i]+"\n"

                f.write(newline)

    @staticmethod
    def goodline(line, preferences, utilities):
        normalizingItemPosition = max(int(math.floor(len(utilities) * PARAMETERS.NORMALIZING_ITEM_POSITION)), 0)
        normalizingItem = utilities[normalizingItemPosition]
        probs = [min(PARAMETERS.MAX_PROB_VALUE_SELECTION,float(i)/normalizingItem) for i in utilities]
        valuesSelected = []
        for i in range(len(preferences)):
            if random.random()< probs[i]:
                valuesSelected.append(preferences[i])

        old_line_data = line.split('\t')
        newline = old_line_data[0]+'\t'+old_line_data[1]
        for v in valuesSelected:
            newline += '\t'+str(v)
        newline += '\t'+old_line_data[-2]+'\t'+old_line_data[-1]+'%GoodLine'

        return newline

    @staticmethod
    def badline(line, preferences, utilities):
        normalizingItemPosition = len(utilities)-max(int(math.floor(len(utilities) * PARAMETERS.NORMALIZING_ITEM_POSITION)), 0)
        normalizingItem = utilities[normalizingItemPosition]
        probs = [min(min(1, normalizingItem/float(i)), PARAMETERS.MAX_PROB_VALUE_SELECTION) for i in utilities]
        valuesSelected = []
        for i in range(len(preferences)):
            if random.random() < probs[i]:
                valuesSelected.append(preferences[i])

        old_line_data = line.split('\t')
        newline = old_line_data[0] + '\t' + old_line_data[1]
        for v in valuesSelected:
            newline += '\t' + str(v)
        newline += '\t' + old_line_data[-2] + '\t' + old_line_data[-1]+'%BadLine'

        return newline

    @staticmethod
    def trimLongestCATS(cats1, cats2):
        f1 = open(cats1, "r")
        f2 = open(cats2, "r")
        lines1 = f1.readlines()
        lines2 = f2.readlines()
        f1.close()
        f2.close()
        if len(lines1) != len(lines2):
            length = min(len(lines1), len(lines2))
            f1 = open(cats1, "w")
            f2 = open(cats2, "w")
            for i in range(length):
                f1.write(lines1[i])
                f2.write(lines2[i])
            f1.close()
            f2.close()

    @staticmethod
    def readPreferences(prefFile):
        f = open(prefFile, "r")
        lines = f.readlines()
        read = False
        prefs = {}
        for l in lines:
            if read:
                if len(l.strip("\n").split(" "))==3:
                    data = l.strip("\n").split(" ")
                    val = int(data[0])
                    num = float(data[2])
                    prefs[val] = num
            if l[:7] == "Utility":
                read = True
        return prefs

    @staticmethod
    def orderedValues(preferences):
        utilities = []
        for i in range(len(preferences)):
            utilities.append(preferences[i])
        valuePreference =range(len(preferences))
        valuePreference = [x for _, x in sorted(zip(utilities, valuePreference),reverse=True)]
        utilities = sorted(utilities, reverse=True)
        return valuePreference, utilities


def main():
    pb = ProblemSolver()
    if PARAMETERS.ORDER_BUILD <= PARAMETERS.ORDER_SOLVE and PARAMETERS.BUILD:
        print "--------------BUILDING LP FILES--------------"
        pb.build()
        if PARAMETERS.SOLVE:
            print "--------------SOLVING PROBLEMS--------------"
            pb.solve()

    elif PARAMETERS.SOLVE:
        print "--------------SOLVING PROBLEMS--------------"
        pb.solve()
        if PARAMETERS.BUILD:
            print "--------------BUILDING LP FILES--------------"
            pb.build()
    elif PARAMETERS.BUILD:
        print "--------------BUILDING LP FILES--------------"
        pb.build()

if __name__ == "__main__":
    main()
