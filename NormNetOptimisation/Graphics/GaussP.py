import numpy as np
import matplotlib.pyplot as pl
import matplotlib.patches as mpatches

# Define the kernel function
def kernel(a, b, param):
    sqdist = np.sum(a**2,1).reshape(-1,1) + np.sum(b**2,1) - 2*np.dot(a, b.T)
    return np.exp(-.5 * (1/param) * sqdist)

def main(x, X, y, Y, lowbud = None, medbud= None, highbud = None, size = 10):
    # Test data
    n = 500
    Xtest = np.linspace(0, max(x), n).reshape(-1,1)

    param = 0.1
    K_ss = kernel(Xtest, Xtest, param)

    # Noiseless training data
    Xtrain = X.reshape(len(x),1)
    ytrain = Y

    # Apply the kernel function to our training points
    K = kernel(Xtrain, Xtrain, param)
    L = np.linalg.cholesky(K + 0.00005*np.eye(len(Xtrain)))

    # Compute the mean at our test points.
    K_s = kernel(Xtrain, Xtest, param)
    Lk = np.linalg.solve(L, K_s)
    mu = np.dot(Lk.T, np.linalg.solve(L, ytrain)).reshape((n,))

    # Compute the standard deviation so we can plot it
    s2 = np.diag(K_ss) - np.sum(Lk**2, axis=0)
    stdv = np.sqrt(s2)
    # Draw samples from the posterior at our test points.
    L = np.linalg.cholesky(K_ss + 1e-6*np.eye(n) - np.dot(Lk.T, Lk))
    f_post = mu.reshape(-1,1) + np.dot(L, np.random.normal(size=(n,3)))
    lowpoints, medpoints, highpoints = None, None, None
    if lowbud and medbud and highbud:
        plotpoints = min(len(lowbud[0]),len(medbud[0]),len(highbud[0]))
        for r in range(plotpoints):
            lowpoints = pl.scatter(lowbud[0][r], lowbud[1][r], label= "Low budget", s = size, c="orange")
            medpoints = pl.scatter(medbud[0][r], medbud[1][r], label="Medium budget", s=size, c= "yellow")
            highpoints = pl.scatter(highbud[0][r], highbud[1][r], label="High budget", s=size, c="blue")
    else:
        pl.scatter(Xtrain, ytrain, c = "blue", s = size)
    #pl.plot(Xtest, f_post)
    pl.gca().fill_between(Xtest.flat, mu-2*stdv, mu+2*stdv, color="#dddddd")
    pl.plot(Xtest, mu, c ="red", lw=2, label = "Regression line")
    if lowpoints and medpoints and highpoints:
        red_patch = mpatches.Patch(color='red', label='Regression line')
        pl.legend(handles=[lowpoints, medpoints, highpoints, red_patch])
    pl.show()
