from DecidimProblemBuilder import DecidimProblemBuilder
from DecidimOptim import DecidimOptim
from ExperimentPlot import ExperimentPlot
import os

class DecidimPlot:

    def __init__(self):
        self.original = None
        self.minb = None
        self.maxb = None
        self.increment = None
        self.inputnames = None
        self.outputSupAct = None
        self.outputCostAct = None
        self.outputSupOpt = None
        self.outputCostOpt = None
        self.outputNumOpt = None
        self.outputNumAct = None
        self.origbud = None
        self.weights = []
        self.folder = ""
        self.plotsfolder =""
        self.buildpath = None

    def build(self):
        wd = os.getcwd()
        self.folder = wd +"/ExperimentData/"+raw_input("Name (Eixample/Gracia):\n")+"/"
        self.plotsfolder = self.folder+"Plots/"
        self.original = self.folder + self.folder.split("/")[-2] + ".txt"
        self.minb = int(raw_input("Minimum budget to check:\n"))
        self.maxb = int(raw_input("Maximum budget to check:\n"))
        self.origbud = int(raw_input("Original budget:\n"))
        self.increment = int(raw_input("Increments to make:\n"))
        self.config = raw_input("Config:\n")
        self.buildpath = self.folder+self.config+"Data/"


        if self.config == "I":
            self.weights = [0.5, 0.5]
            self.outputSupAct = self.folder+"Actual/DATA_SUP_I.txt"
            self.outputCostAct = self.folder+"Actual/DATA_COST_I.txt"
            self.outputCostOpt = self.folder+"Optimal/DATA_COST_I.txt"
            self.outputSupOpt = self.folder+"Optimal/DATA_SUP_I.txt"
            self.outputNumOpt = self.folder+"Optimal/DATA_NUM_I.txt"
            self.outputNumAct = self.folder+"Actual/DATA_NUM_I.txt"
        elif self.config == "Op":
            self.weights = [0.7, 0.3]
            self.outputSupAct = self.folder+"Actual/DATA_SUP_Op.txt"
            self.outputCostAct = self.folder+"Actual/DATA_COST_Op.txt"
            self.outputCostOpt = self.folder+"Optimal/DATA_COST_Op.txt"
            self.outputSupOpt = self.folder+"Optimal/DATA_SUP_Op.txt"
            self.outputNumOpt = self.folder + "Optimal/DATA_NUM_Op.txt"
            self.outputNumAct = self.folder + "Actual/DATA_NUM_Op.txt"
        elif self.config == "Co":
            self.weights = [0.3, 0.7]
            self.outputSupAct = self.folder+"Actual/DATA_SUP_Co.txt"
            self.outputCostAct = self.folder+"Actual/DATA_COST_Co.txt"
            self.outputCostOpt = self.folder+"Optimal/DATA_COST_Co.txt"
            self.outputSupOpt = self.folder+"Optimal/DATA_SUP_Co.txt"
            self.outputNumOpt = self.folder + "Optimal/DATA_NUM_Co.txt"
            self.outputNumAct = self.folder + "Actual/DATA_NUM_Co.txt"
        elif self.config == "ECo":
            self.weights = [0, 1]
            self.outputSupAct = self.folder+"Actual/DATA_SUP_ECo.txt"
            self.outputCostAct = self.folder+"Actual/DATA_COST_ECo.txt"
            self.outputCostOpt = self.folder+"Optimal/DATA_COST_ECo.txt"
            self.outputSupOpt = self.folder+"Optimal/DATA_SUP_ECo.txt"
            self.outputNumOpt = self.folder + "Optimal/DATA_NUM_ECo.txt"
            self.outputNumAct = self.folder + "Actual/DATA_NUM_ECo.txt"
        elif self.config == "EOp":
            self.weights = [1, 0]
            self.outputSupAct = self.folder+"Actual/DATA_SUP_EOp.txt"
            self.outputCostAct = self.folder+"Actual/DATA_COST_EOp.txt"
            self.outputCostOpt = self.folder+"Optimal/DATA_COST_EOp.txt"
            self.outputSupOpt = self.folder+"Optimal/DATA_SUP_EOp.txt"
            self.outputNumOpt = self.folder + "Optimal/DATA_NUM_EOp.txt"
            self.outputNumAct = self.folder + "Actual/DATA_NUM_EOp.txt"
        else:
            return
        dpb = DecidimProblemBuilder(self.original, self.minb, self.maxb, self.increment, self.weights, self.buildpath)
        dpb.build()
        self.inputnames = dpb.getFileNames()
        return dpb.getFileNames()

    def solve(self, filename, outputs = None):
        problem = DecidimOptim()
        problem.readData(filename)
        problem.writeLP()
        problem.solve(outputSup=outputs[0], outputCost=outputs[1], outputNum = outputs[2])
        problem.solveNoOptimisation(outputSup=outputs[3], outputCost=outputs[4], outputNum = outputs[5])
        problem.show_info()

    def plot(self):
        ep = ExperimentPlot(self.outputSupAct, self.outputSupOpt, "sup")
        ep.plot(True, self.plotsfolder+"Sup"+self.config+".png", self.origbud)
        ep = ExperimentPlot(self.outputCostAct, self.outputCostOpt, "cost")
        ep.plot(True, self.plotsfolder+"Cost"+self.config+".png", self.origbud)
        ep = ExperimentPlot(self.outputNumAct, self.outputNumOpt, "num")
        ep.plot(True, self.plotsfolder + "Num" + self.config + ".png", self.origbud)

    def getOutputs(self):
        return self.outputSupOpt, self.outputCostOpt, self.outputNumOpt, self.outputSupAct, self.outputCostAct, self.outputNumAct

def main():
    dp = DecidimPlot()
    inputnames = dp.build()
    outputnames = dp.getOutputs()
    outputs = [open(outputnames[0], "w"), open(outputnames[1], "w"), open(outputnames[2], "w"), open(outputnames[3], "w"), open(outputnames[4], "w"), open(outputnames[5], "w")]
    for filename in inputnames:
        dp.solve(filename, outputs)
    for f in outputs:
        f.close()
    dp.plot()


if __name__ == "__main__":
        main()