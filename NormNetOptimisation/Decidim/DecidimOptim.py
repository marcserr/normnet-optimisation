import time
import cplex

class DecidimOptim:

    def __init__(self):
        self.input = None #The name of the raw data input file
        self.outputName = None #The name of the output file
        self.output = None #The oputput file itself
        self.proposals = [] #The list of proposals
        self.supports = [] #The list of supports of the proposals
        self.costs = [] #The list of the costs of the proposals
        self.weights = [] #The list of the weights of the criteria(1st pos:community opinion, 2nd pos: cost)
        self.budget = None #The maximum budget allowed to spend
        self.info = ""


    def readData(self, dfilepath):
        #Reads raw data from a file

        self.input = open(dfilepath, "r")
        reading = None
        for line in self.input:
            if line[-1] == "\n":
                line = line[:-1]
            if line in ["Proposals", "Supports", "Costs", "Budget", "Weights"]:
                reading = line
            elif line != "":
                if reading == "Proposals":
                    self.proposals.append(line)
                elif reading == "Supports":
                    self.supports.append(int(line))
                elif reading == "Costs":
                    self.costs.append(int(line))
                elif reading == "Budget":
                    self.budget = int(line)
                elif reading == "Weights":
                    self.weights.append(float(line))
        self.input.close()

        self.outputName = dfilepath.replace(".txt", ".lp")

    def writeLP(self, LPfilename = None, rels = False, values = False):
        #Writes the LP file, the file that the optimiser can read and solve
        if LPfilename:
            self.outputName = LPfilename
        self.output = open(self.outputName, "w")
        if self.budget and self.proposals:
            if len(self.proposals) == len(self.supports) and len(self.proposals) == len(self.costs):
                self.output.write("Maximize\n\n")
                self.writeOptFunction(values)
                self.output.write("\n\nSubject To\n\n")
                self.writeConstraints(rels)
                self.output.write("\nBinaries\n\n")
                self.writeBinaries()
                self.output.write("\nEnd")
            else:
                print "Missing input data"
        else:
            print "No input"
        self.output.close()

    def writeOptFunction(self, values):
        #Writes the maximisation function of the optimisation process

        if(values):
            pass #TODO:Future project
        else:
            maxSupport = max(self.supports)
            w_s = self.weights[0]
            w_c = self.weights[1]
            for i in range(len(self.supports)):
                supportParameter = (w_s*float(self.supports[i]))/float(maxSupport)
                costParameter = (w_c*float(self.costs[i]))/float(self.budget)
                if i != 0:
                    self.output.write(' + ')
                self.output.write('{0:.8f}'.format(float(supportParameter)) + ' p_' + str(i+1) + ' - ' + '{0:.8f}'.format(float(costParameter)) + ' p_' + str(i+1))
            self.output.write(' + ' + str(w_c) + ' y')


    def writeConstraints(self, rels):
        #Writes constraints to the LP, that is incompatibilities between proposals, proposal generalisation
        # and budget limitations

        if(rels):
            pass #TODO:Needs especification in input file

        #Selected norms cannot exceed budget
        for i in range(len(self.costs)):
            if i!= 0:
                self.output.write(" + ")
            self.output.write(str(self.costs[i])+ " p_"+str(i+1))
        self.output.write(" <= "+str(self.budget)+"\n")

        #Minimisation of budget through maximisation of formula and this constraint

        for i in range(len(self.proposals)):
            if i != 0:
                self.output.write(' + ')
            self.output.write('p_' + str(i+1))
        self.output.write(' - y >= ' + '0\n')

        for i in range(len(self.proposals)):
            if i != 0:
                self.output.write(' + ')
            self.output.write('p_' + str(i+1))
        self.output.write(' ' + str(-len(self.proposals) - 1) + ' y <= 0\n')

    def writeBinaries(self):
        n=1
        for p in self.proposals:
           self.output.write("p_"+str(n)+"\n")
           n += 1
        self.output.write("y"+ "\n")

    def solve(self, problem_lp = None, problem_sol = None, timeLim = 3600, outputSup = None, outputCost = None, outputNum = None, show = False):
        # Receives a LP file and solves it. It exports the answer to a .sol file and also shows it.
        # You can specify the time limit with timeLim.

        if not problem_lp:
            problem_lp = self.outputName
        if not problem_sol:
            problem_sol = self.outputName.replace(".lp", ".sol")

        try:

            m = cplex.Cplex(problem_lp)

            m.parameters.timelimit.set(timeLim)
            start_time = time.time()
            m.solve()
            final_time = time.time() - start_time

            m.solution.write(problem_sol)

            print 100 * '_'
            print '\n'
            print 'Solved in ' + str(final_time) + ' seconds.'

            if 'w_v' in m.variables.get_names():
                print 'Solved VMPNSPLB problem. Here the results:'
            elif 'y' in m.variables.get_names():
                print 'Solved MPNSPLB problem. Here the results:'
            else:
                print 'Solved MPNSP problem. Here the results:'

            y_name = len(m.solution.get_values())-1
            selected = []
            not_selected = []
            spent = 0
            total_support = 0
            for varName, varValue in enumerate(m.solution.get_values()):
                if varName != y_name:
                    if varValue == 1:
                        status = "Accepted"
                        selected.append("p_"+str(varName+1))
                        spent += self.costs[varName]
                        total_support += self.supports[varName]
                    else:
                        status = "Rejected"
                        not_selected.append("p_" + str(varName + 1))
                    print "p_"+str(varName+1) + ' : ' + status
            outputSup.write(str(self.budget) + "," + str(total_support)+"\n")
            outputCost.write(str(self.budget) + "," + str(spent)+"\n")
            outputNum.write(str(self.budget) + "," + str(len(selected)) + "\n")


            if show:
                self.info += "\n-----------------\nOptimal solution:\n-----------------\n\n"
                self.info += "Selected proposals: " + str(selected)+"\n"
                self.info += "Not selected proposals: " + str(not_selected)+"\n"
                self.info += "Number of selected proposals: " + str(len(selected)) + "\n"
                self.info += "Support for selected proposals: " + str(total_support)+"\n"
                self.info += "Cost of selected proposals: " + str(spent)+" ("+str(float(spent)/float(self.budget)*100)+"%)\n"

        except cplex.exceptions.CplexError:
            outputSup.write(str(self.budget) + "," + str(0)+"\n")
            outputCost.write(str(self.budget) + "," + str(0)+"\n")
            outputNum.write(str(self.budget) + "," + str(0) + "\n")
            print 'Sorry'

    def solveNoOptimisation(self, outputSup = None, outputCost = None, outputNum = None, show = False):
        zipped = zip(self.supports, self.proposals, self.costs)
        zipped.sort(reverse=True)
        self.supports, self.proposals, self.costs = zip(*zipped)
        spent = 0
        total_support = 0
        selected = []
        not_selected = []
        i = 0
        while i < len(self.proposals) and spent+self.costs[i] < self.budget:
            selected.append(self.proposals[i])
            total_support += self.supports[i]
            spent += self.costs[i]
            i += 1
        while i < len(self.proposals):
            not_selected.append(self.proposals[i])
            i += 1
        outputSup.write(str(self.budget)+","+str(total_support)+"\n")
        outputCost.write(str(self.budget)+","+str(spent)+"\n")
        outputNum.write(str(self.budget) + "," + str(len(selected)) + "\n")
        if show:
            self.info += "\n----------------\nActual solution:\n----------------\n\n"
            self.info += "Selected proposals: "+str(selected)+"\n"
            self.info += "Not selected proposals: "+str(not_selected)+"\n"
            self.info += "Number of selected proposals: " + str(len(selected)) + "\n"
            self.info += "Support for selected proposals: "+str(total_support)+"\n"
            self.info += "Cost of selected proposals: "+str(spent)+" ("+str(float(spent)/float(self.budget)*100)+"%)\n"

    def show_info(self):
        print self.info

    def erase_info(self):
        self.info = ""

def main():
    problem = DecidimOptim()
    datafile = None
    while not datafile:
        datafile = raw_input("Input file for optimisation?\n")
        if datafile.count(".") != 1:
            datafile = None
            print "Incorrect file name, name should be something like file.txt"
    problem.readData(datafile)
    problem.writeLP()
    problem.solve(show = True)
    problem.solveNoOptimisation(show = True)
    problem.show_info()

if __name__ == "__main__":
    main()
