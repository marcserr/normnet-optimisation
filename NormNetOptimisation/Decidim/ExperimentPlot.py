import matplotlib.pyplot as plt
import os

class ExperimentPlot:

    def __init__(self, input1, input2, type):
        self.input1 = open(input1, "r")
        self.input2 = open(input2, "r")
        self.type = type


    def plot(self, show = True, output = None, originalBud = None):
        x1 = []
        y1 = []
        x2 = []
        y2 = []
        for line in self.input1:
            el = line.split(",")
            x1.append(float(el[0]))
            y1.append(float(el[1]))

        for line in self.input2:
            el = line.split(",")
            x2.append(float(el[0]))
            y2.append(float(el[1]))

        if originalBud:
            y3 = range(0, int(max(y1)), 1)
            x3 = [originalBud]*len(y3)
            plt.plot(x3, y3, color="purple")

        plt.plot(x1, y1, '-o', color = "red", label = "Actual")
        plt.plot(x2, y2, '-o', color = "blue", label = "Optimal")
        if self.type == "sup":
            plt.xlabel("Avaliable budget")
            plt.ylabel("Supports of the selected proposals")
        elif self.type == "cost":
            plt.xlabel("Avaliable budget")
            plt.ylabel("Spent money")
        elif self.type == "num":
            plt.xlabel("Avaliable budget")
            plt.ylabel("Number of proposals selected")
        plt.legend(loc = 'lower right')
        if output:
            plt.savefig(output)
        if show:
            plt.show()

def main():
    print os.getcwd()
    i1 = "/Users/Marc/PycharmProjects/NormNetOptimisation/Decidim/ExperimentData/Gracia/Actual/DATA_SUP_I.txt"
    i2 = "/Users/Marc/PycharmProjects/NormNetOptimisation/Decidim/ExperimentData/Gracia/Optimal/DATA_SUP_I.txt"
    ep = ExperimentPlot(i1, i2)
    ep.plot()


if __name__ == "__main__":
        main()