
class DecidimProblemBuilder:

    def __init__(self, original, minb, maxb, increment, weights, buildpath):
        self.minbudget = minb
        self.maxbudget = maxb
        self.increment = increment
        self.originalPath = original
        self.buildPath = buildpath
        self.names = []
        self.weights = weights

    def build(self):
        originalFile = open(self.originalPath, "r")
        fileContent = originalFile.readlines()
        originalFile.close()
        budgets = range(self.minbudget, self.maxbudget, self.increment)
        budgets.append(self.maxbudget)
        self.buildPath = self.buildPath+self.originalPath.split("/")[-1]
        for bud in budgets:
            fileContent[-1] = str(bud)
            fileContent[2] = str(self.weights[0])+"\n"
            fileContent[3] = str(self.weights[1])+"\n"
            newfilename = self.buildPath.replace(".txt", str(bud)+".txt")
            self.names.append(newfilename)
            newfile = open(newfilename, "w+")
            for line in fileContent:
                newfile.write(line)
            newfile.close()

    def getFileNames(self):
        return self.names

def main():
    datafile = raw_input("Original file:\n")
    minb = raw_input("Minimum budget to check:\n")
    maxb = raw_input("Maximum budget to check:\n")
    increment = raw_input("Increments to make:\n")
    weight1 = float(raw_input("Support weight:\n"))
    weight2 = float(raw_input("Cost weight:\n"))
    builder = DecidimProblemBuilder(datafile, minb, maxb, increment,[weight1, weight2] )
    builder.build()


if __name__ == "__main__":
        main()