import os
import json

class CommentEvaluator:

    def __init__(self, lowerBound, upperBound, alpha, tol):
        self.lb = lowerBound
        self.ub = upperBound
        self.alpha = alpha
        self.tol = tol

    def evaluate_comment(self, votes):
        result = 0
        for v in votes:
            result += (self.importanceFunction(v)*v)/len(votes)
        return result

    def evaluate_set(self, ComEval, numVotes):
        alphaVotes = []
        for i in range(0, len(ComEval)):
            if ComEval[i] > (self.lb+self.ub)/2:
                if numVotes[i] >= self.alpha * max(numVotes):
                    alphaVotes.append(ComEval[i])
        result = 0
        for v in alphaVotes:
            result += (self.importanceFunction(v)*v)/len(alphaVotes)
        return result

    def evaluate_proposal(self, negEval, posEval, posVotes, negVotes):
        if posVotes+negVotes > 0:
            result = ((self.ub-negEval+self.lb)*negVotes+posEval*posVotes)/(posVotes+negVotes)
        else:
            result = 0
        return result

    def importanceFunction(self, x):
        if x <= (self.ub + 3. * self.lb) / 4. :
            result = (-4. * self.lb * self.lb-1.8 * self.lb * self.ub+1. * self.ub * self.ub+9.8 * self.lb * x-0.2 * self.ub * x-4.8 * x * x) / ((self.lb-1. * self.ub) * (self.lb-1. * self.ub))
        elif x < (3. * self.ub+5. * self.lb) / 8. :
            result = (-1.75 * self.lb-1.45 * self.ub+3.2 * x) / (self.lb-1. * self.ub)
        elif x <= (5. * self.ub+3. * self.lb) / 8. :
            result = (4. * self.lb * self.lb+8. * self.lb * self.ub+4. * self.ub * self.ub-16. * self.lb * x-16. * self.ub * x+16. * x * x) / ((self.lb-1. * self.ub) * (self.lb-1. * self.ub))
        elif x < (3. * self.ub+self.lb) / 4. :
            result = (1.45 * self.lb+1.75 * self.ub-3.2 * x) / (self.lb-1. * self.ub)
        elif x <= self.ub+ self.tol:
            result = (1. * self.lb * self.lb-1.8 * self.lb * self.ub-4. * self.ub * self.ub-0.2 * self.lb * x+9.8 * self.ub * x-4.8 * x * x) / ((self.lb-1. * self.ub) * (self.lb-1. * self.ub))
        else:
            result = None
            print("x value out of spectrum")

        return result

class JSONEvaluator:

    def __init__(self, inputDir, output):
        self.files = os.listdir(inputDir)
        self.output = open(output, "w")


    def evaluatAllFiles(self):
        for f in self.files:
            json_data = open(os.getcwd()+"/comments/"+f).read()
            data = json.loads(json_data)
            if data["comments"]:
                eval = CommentEvaluator(1., 5., 0.3, 0.000000001)
                self.output.write(f+":\n")
                negComEval = []
                posComEval = []
                posComVotes = []
                negComVotes = []
                for com in data["comments"]:
                    likes = int(com["total_likes"])
                    dislikes = int(com["total_dislikes"])
                    evaluation = eval.evaluate_comment([5.]*likes+[1.]*dislikes)
                    if com["alignment"]:
                        posComEval.append(evaluation)
                        posComVotes.append(likes+dislikes)
                    else:
                        negComEval.append(evaluation)
                        negComVotes.append(likes + dislikes)
                    self.output.write("comment"+str(com["id"])+": pos: "+str(likes)+" neg: "+str(dislikes)+" eval: "+str(evaluation)+"\n")
                posSetEval = eval.evaluate_set(posComEval, posComVotes)
                negSetEval = eval.evaluate_set(negComEval, negComVotes)
                if posSetEval:
                    self.output.write("Positive Comments eval: "+str(posSetEval)+"\n")
                else:
                    self.output.write("Positive Comments eval: Not possible\n")
                if negSetEval:
                    self.output.write("Negative Comments eval: " + str(negSetEval)+"\n")
                else:
                    self.output.write("Negative Comments eval: Not possible\n")
                propEval = eval.evaluate_proposal(posSetEval, negSetEval, sum(posComVotes), sum(negComVotes))
                if propEval:
                    self.output.write("Proposal eval: " + str(propEval)+"\n")
                else:
                    self.output.write("Proposal eval: Not possible\n")

    def close_output(self):
        self.output.close()

def main():
    JE = JSONEvaluator(os.getcwd()+"/comments", "output.txt")
    JE.evaluatAllFiles()
    JE.close_output()


if __name__ == "__main__":
    main()