import json
import sys

from gurobipy import *

def getNonGeneralised(P,NN):

# Same name as in the paper
# Gives you the subset of norms not generalised by any other norm

    G = NN.copy()
    for i in NN:
        for j in NN[i]:
            if j in G:
                del G[j]


    return G



def getSiblingGroups(P, NN):

#  Same name as in the paper
#  Returns  the groups of siblings of norms that are not in P

    sibList={}

    for i in NN:
        if i not in P:
            sibList[i] = NN[i]

    return sibList





def GENERALISATIONCOMP(NN):

# Same name as the paper  Algorithm 1
# The algorithm does exactly the same

    N =   {i:0 for i in NN}

    g = {}
    P = N.copy()
    G =  getNonGeneralised(P,NN)
    
    for i  in G:
        g[i] = 1
        del P[i]

    while P:
        sibList = getSiblingGroups(P,NN)
    
        for ps in sibList:
            for i in sibList[ps]:
                g[i] = float(g[ps])/len(sibList[ps])
                if i in P:
                    del P[i]

   
    return g

def recurIncl(NN,i):

# This is the method described in the section 3.2.1
# A recursive method to find the inclusion power
    if len(NN[i])==0:
        return 1
    else:
        r = 1
        
        for j in NN[i]:
            r = r + recurIncl(NN,j)
        
        return r

def INCLUSIONCOMP(NN):

# Applies the previous method to each norm we have

    r = {}

    for i in NN:
        r[i] = recurIncl(NN,i)

    
    return r


def recAnc(NN,j):

# I thought this method myself. It simply mimicks the one to calculate the inclusion power
# It also is recursive
# You get the ancestors of a norm j

    r=[j]
    
    for  i in NN:
        if len(NN[i])>0:
            if j in NN[i]:
                r = r+recAnc(NN,i)
        
    return r

def ANCESTORS(NN):

# Applies the previous method to each norm we have

    a = {}
    
    for i in NN:
        a[i] = recAnc(NN,i)
        del a[i][0]
        a[i].sort()
    
        
        if len(a[i])>0:
            for j in a[i]:
                if i in NN[j]:
                    a[i].remove(j)


   
    return a


def utilityValue(i,V):

# Calculates the utility value number of a specific value i
	u=1
	
	for j in range(len(V)):
		if V[j]==i:
			break
	
	for k in V[j+1:]:
		u= u + utilityValue(k,V)

	
	return u			
	
def uCalculator(V):

# Calculates the utility value number of every value
	u={}

	for v in V:
		u[v] = utilityValue(v,V)

	return u

def u_nCalculator(V,Values):

# Calculates the value support of every norm
	u = uCalculator(V)
	
	print u 

	u_n = { }

	for norm in Values:
		u_n[norm] = 0

		for v in Values[norm]:
			u_n[norm] = u_n[norm] + u[v]

	return u_n	

# We open the json file

normFile = open('Norms.json',"r")
norms = json.load(normFile)

# The names given now are based in the ones in the paper

# NN contains every norm and the children of each one

NN =  norms["NN"]

# R_x contains the pairs of incompatible norms
R_x = norms["R_x"]

# S contains the pairs of substitutable norms
S = norms["S"]




# b is the budget
b = int(norms["budget"])

# c contains the cost of every norm
c = norms["costs"]

# V contains every value
V = norms["V"]

# Values contains the values associated to each norm
Values = norms["Values"]


# Now we calculate the ancestors
A   = ANCESTORS(NN)

# We only need the inclusion power or the generalisation power. We simply calculate both to see if everything is fine
r_I = INCLUSIONCOMP(NN)
r_G = GENERALISATIONCOMP(NN)




# Finally we get the value support of each norm

u_n = u_nCalculator(V, Values)

# For instance, we take the inclusion power

r = r_I

# Now we calculate R_max. The method is the same you can read in Part 4
N =   {i:0 for i in NN}
P = N.copy()
G =  getNonGeneralised(P,NN)
        
R_max = 0
        
for j in G:
    R_max = R_max + r[j]

# Then we get V_max using the method described in Part 5
V_max = 0

for i in u_n:
    V_max = V_max + u_n[i]

w_r = 0
w_c = 0
w_v = 1

# Here starts the gurobi part

try:

	#We create a new model

	m = Model("modelo")

	#We create the variables

	vars={}

	for norm in NN:
		vars[norm] = m.addVar(vtype=GRB.BINARY,name=norm)

	y = m.addVar(vtype=GRB.BINARY,name='y')

	#Integrate them

	m.update()

	#Set objective


	m.setObjective((w_r/R_max)*quicksum(vars[norm]*r[norm] for norm in vars)+w_c*(y-1./b*quicksum(vars[norm]*c[norm] for norm in vars ))+ (w_v/V_max)*quicksum(vars[norm]*u_n[norm] for norm in vars ),GRB.MAXIMIZE)

	#We add the constraints
    #The number for each one matches the number given in the paper

	# Const 2
	for norm in vars:
		for child in NN[norm]:
			m.addConstr(vars[norm] + vars[child] <= 1 )
	
	# Const 3	
	for norm in vars:
		m.addConstr(quicksum(vars[child] for child in NN[norm] )   <= len(NN[norm])-1 )
	

	# Const 4
	for norm in vars:
		for ancestor in A[norm]:
			m.addConstr(vars[norm] + vars[ancestor] <= 1 )

		
	# Const 5
	for couple in R_x:
		m.addConstr(vars[couple[0]] + vars[couple[1]] <= 1)


	# Const 6
	for couple in S:
		m.addConstr(vars[couple[0]] + vars[couple[1]] <= 1)
	

	# Const 9
	m.addConstr(quicksum(vars[norm]*c[norm] for norm in vars) <= b)

	# Const 12
	m.addConstr(y <= quicksum(vars[norm] for norm in vars) )
	m.addConstr(quicksum(vars[norm] for norm in vars) <= (len(vars)+1)*y)

    # Optimize and get the solution
	m.optimize()

	for v in m.getVars():
		print('%s %g' % (v.varName, v.x))

	print('Obj: %g' % m.objVal)

except GurobiError:
	print('Error reported')

