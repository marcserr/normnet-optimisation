import json
import sys

from gurobipy import *

def getNonGeneralised(P,NN):

# Same name as in the paper
# Gives you the subset of norms not generalised by any other norm

    G = NN.copy()
    for i in NN:
        for j in NN[i]:
            if j in G:
                del G[j]


    return G



def getSiblingGroups(P, NN):

#  Same name as in the paper
#  Returns  the groups of siblings of norms that are not in P

    sibList={}

    for i in NN:
        if i not in P:
            sibList[i] = NN[i]

    return sibList





def GENERALISATIONCOMP(NN):

# Same name as the paper  Algorithm 1
# The algorithm does exactly the same

    N =   {i:0 for i in NN}

    g = {}
    P = N.copy()
    G =  getNonGeneralised(P,NN)
    
    for i  in G:
        g[i] = 1
        del P[i]

    while P:
        sibList = getSiblingGroups(P,NN)
    
        for ps in sibList:
            for i in sibList[ps]:
                g[i] = float(g[ps])/len(sibList[ps])
                if i in P:
                    del P[i]


    return g

def recurIncl(NN,i):

# This is the method described in the section 3.2.1
# A recursive method to find the inclusion power

    if len(NN[i])==0:
        return 1
    else:
        r = 1
        
        for j in NN[i]:
            r = r + recurIncl(NN,j)
        
        return r

def INCLUSIONCOMP(NN):
    
# Applies the previous method to each norm we have

    r = {}

    for i in NN:
        r[i] = recurIncl(NN,i)


    return r


def recAnc(NN,j):

# I thought this method myself. It simply mimicks the one to calculate the inclusion power
# It also is recursive
# You get the ancestors of a norm j

    r=[j]
    
    for  i in NN:
        if len(NN[i])>0:
            if j in NN[i]:
                r = r+recAnc(NN,i)
        
    return r

def ANCESTORS(NN):
    
# Applies the previous method to each norm we have

    a = {}
    
    for i in NN:
        a[i] = recAnc(NN,i)
        del a[i][0]
        a[i].sort()
    
        
        if len(a[i])>0:
            for j in a[i]:
                if i in NN[j]:
                    a[i].remove(j)


    print a
    return a


# We open the json file

normFile = open('Norms.json',"r")
norms = json.load(normFile)

# The names given now are based in the ones in the paper

# NN contains every norm and the children of each one

NN =  norms["NN"]

# R_x contains the pairs of incompatible norms
R_x = norms["R_x"]

# S contains the pairs of substitutable norms
S = norms["S"]



# Now we calculate the ancestors
A   = ANCESTORS(NN)

# We only need the inclusion power or the generalisation power. We simply calculate both to see if everything is fine
r_I = INCLUSIONCOMP(NN)
r_G = GENERALISATIONCOMP(NN)

# For instance, we take the generalisation power

r = r_G

try:

	#We create a new model

	m = Model("modelo")

	#We create the variables

	vars={}

	for norm in NN:
		vars[norm] = m.addVar(vtype=GRB.BINARY,name=norm)

	#Integrate them

	m.update()

	#Set objective

	m.setObjective(quicksum(vars[norm]*r[norm] for norm in vars),GRB.MAXIMIZE)

    #We add the constraints
    #The number for each one matches the number given in the paper

	# Const 2
	for norm in vars:
		for child in NN[norm]:
			m.addConstr(vars[norm] + vars[child] <= 1 )
	
	# Const 3	
	for norm in vars:
		m.addConstr(quicksum(vars[child] for child in NN[norm] )  <= len(NN[norm])-1 )
	

	# Const 4
	for norm in vars:
		for ancestor in A[norm]:
			m.addConstr(vars[norm] + vars[ancestor] <= 1 )

		
	# Const 5
	for couple in R_x:
		m.addConstr(vars[couple[0]] + vars[couple[1]] <= 1)


	# Const 6
	for couple in S:
		m.addConstr(vars[couple[0]] + vars[couple[1]] <= 1)


    # Optimize and get the solution

   	m.optimize()

	for v in m.getVars():
		print('%s %g' % (v.varName, v.x))

	print('Obj: %g' % m.objVal)

except GurobiError:
	print('Error reported')

