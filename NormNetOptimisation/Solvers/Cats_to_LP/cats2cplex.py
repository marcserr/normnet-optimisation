
import argparse

from CATSAuction import CATSAuction
import CPLEXSolver

# Main
##############################################################################

def w(word):
    if word == 'low':
        return 0.25
    elif word == 'medium':
        return 0.5
    elif word == 'high':
        return 0.75
    else:
        return 1.0


def main(opts):
    auction = CATSAuction()
    auction.load_file(opts.auction)
    print ""
    print "Auction loaded"
    print ""
    # auction.display(sys.stdout)
    print ""

    formula = auction.to_LP(False,False)

    print "Generating result file:", opts.result

    formula.write_LP_file(opts.formula, opts.problem, opts.repower, w(opts.p_generalization), float(opts.sigma_g),
                          w(opts.heigth), float(opts.sigma_h), w(opts.width), float(opts.sigma_w), w(opts.budget))

    CPLEXSolver.problem(opts.formula, opts.result, opts.time)


# Script entry point
###############################################################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="", epilog="")

    parser.add_argument('-a', '--auction', type=str, required=True,
                        help="Combinatorial auction file")

    parser.add_argument('-f', '--formula', type=str, required=True,
                        help="Wcnf formula file path")

    parser.add_argument('-r', '--result', type=str, required=True,
                        help="Result file path")

    parser.add_argument('-t', '--time', type=str, required=True,
                        help="Maximum time allowed in seconds")

    parser.add_argument('-p', '--problem', type=str, required=True,
                        help="type of problem. MNSP or MNSPLB")

    parser.add_argument('-rp', '--repower', type=str, required=True,
                        help="representation power. Choose I or G")

    parser.add_argument('-g', '--p_generalization', type=str, required=True,
                        help="percentatge of generalization. Choose low, medium, high or total")

    parser.add_argument('-sg', '--sigma_g', type=str, required=True,
                        help="generalization sigma")

    parser.add_argument('-he', '--heigth', type=str, required=True,
                        help="heigth of trees. Choose low, medium, high or total")

    parser.add_argument('-sh', '--sigma_h', type=str, required=True,
                        help="height sigma")

    parser.add_argument('-w', '--width', type=str, required=True,
                        help="widthh of trees. Choose low, medium, high or total")

    parser.add_argument('-sw', '--sigma_w', type=str, required=True,
                        help="width sigma")

    parser.add_argument('-b', '--budget', type=str, required=True,
                        help="instance budget. Choose low, medium, high or total")

    # parser.add_argument('-s', '--solver', type=str, required=True, help="Path to the WPM3 solver")

    # parser.add_argument('-alo', '--accept-at-least-one', action='store_true', help="Accept at least one bid from each agent")

    # parser.add_argument('-amo', '--accept-at-most-one', action='store_true',help="Accept at most one bid from each agent")

    main(parser.parse_args())
