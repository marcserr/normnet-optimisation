#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import collections
import itertools
import sys
import os

import LPTools

import PARAMETERS

# Immutable type that stores the price and the objects of a bid
Bid = collections.namedtuple('Bid', ['price', 'objects'])


class CATSAuction:

    def __init__(self):
        self.n_bids = 0
        self.n_goods = 0
        self.n_dummies = 0
        self.n_support = 0
        self.n_values = 0
        self.n_val_dummies = 0
        self.total_stake = 0

        self.bids = []
        self.agent_bids = []
        self.supports = []
        self.agent_support = []

    def load_file(self, ex_path, val_path = None):
        """Loads a CATS auction from the given file.
        :param path: Path to the CATS auction file.
        """
        f = open(ex_path, 'r')
        self.load_stream(f)
        f.close()
        if val_path and os.path.exists(val_path):
            f = open(val_path)
            self.load_values(f)
            f.close()

    def load_stream(self, stream):
        """Loads a CATS auction from the given stream."""
        reader = (l for l in map(str.strip, stream) if l and l[0] != '%')

        for values in map(str.split, reader):
            if values[0].lower() == 'goods':
                self.n_goods = int(values[1])
            elif values[0].lower() == 'bids':
                self.n_bids = int(values[1])
            elif values[0].lower() == 'dummy':
                self.n_dummies = int(values[1])
                self.agent_bids = [[] for _ in range(self.n_dummies)]
            else:
                self._parse_bid(values)

    def load_values(self, stream):
        """Loads a CATS auction from the given stream."""
        reader = (l for l in map(str.strip, stream) if l and l[0] != '%')

        for values in map(str.split, reader):
            if values[0].lower() == 'goods':
                self.n_values = int(values[1])
            elif values[0].lower() == 'bids':
                self.n_support = int(values[1])
            elif values[0].lower() == 'dummy':
                self.n_val_dummies = int(values[1])
                self.agent_support = [[] for _ in range(self.n_dummies)]
            else:
                self._parse_bid(values, True)

    def _parse_bid(self, values, val = False):
        price = float(values[1])

        if not val:
            objs = sorted(map(int, itertools.islice(values, 2, len(values) - 1)))
            if objs[-1] < self.n_goods:
                self.agent_bids.append([len(self.bids)])
            else:
                agent_index, objs = objs[-1] - self.n_goods, objs[:-1]
                self.agent_bids[agent_index].append(len(self.bids))

            self.bids.append(Bid(price, frozenset(objs)))
        else:
            val_objs = sorted(map(int, itertools.islice(values, 2, len(values) - 1)))
            if val_objs[-1] < self.n_goods:
                self.agent_support.append([len(self.supports)])
            else:
                val_agent_index, val_objs = val_objs[-1] - self.n_goods, val_objs[:-1]
                self.agent_support[val_agent_index].append(len(self.supports))

            self.supports.append(Bid(price, frozenset(val_objs)))

    def display(self, stream=sys.stdout):
        for ind, a_bids in enumerate(self.agent_bids):
            stream.write('******** Agent {0} ********\n'.format(ind))
            for b_ind, bid in ((b_ind, self.bids[b_ind]) for b_ind in a_bids):
                stream.write("---> Index: "+b_ind+"\n")
                stream.write("     Price: ", bid.price+"\n")
                stream.write("     Objects: "+" ".join(map(str, sorted(bid.objects)))+"\n")

    def to_LP(self, alo_agent_bid, amo_agent_bid, vals_file_name = None):
        formula = LPTools.LPFormula()  # Bid var is bid index + 1
        self._add_LP_formula_header(formula)
        formula.extend_vars(len(self.bids))  # Set bids variables
        num_exc_rels = 0

        if self.supports:

            if vals_file_name:

                vals_file = open(vals_file_name, "w+")

                for ind, bid in enumerate(self.supports, start=0):
                    # Value utility
                    formula.saveValues(ind, bid.objects, vals_file)

                vals_file.close()

        for ind, bid in enumerate(self.bids, start=0):
            # Soft: bid's cost
            formula.add_clause([ind + 1], bid.price)

            # Hard: incompatibility of bids
            for i in range(ind + 1, len(self.bids)):
                if bid.objects & self.bids[i].objects:
                    formula.add_clause([(ind + 1), (i + 1)], PARAMETERS.TOP_WEIGHT)
                    num_exc_rels += 1

        # Hard: at least one & at most one bid from each agent
        for a_bids in self.agent_bids:
            bids_vars = [i + 1 for i in a_bids]
            if alo_agent_bid:
                formula.add_at_least_one(bids_vars, PARAMETERS.TOP_WEIGHT)
            if amo_agent_bid:
                formula.add_at_most_one(bids_vars, PARAMETERS.TOP_WEIGHT)

        return formula, num_exc_rels

    def _add_LP_formula_header(self, formula):
        formula.header.append("CATS auction to LP")
        formula.header.append("--------------------")
        formula.header.append(" # Agents: {0}".format(len(self.agent_bids)))
        formula.header.append(" # Roots: {0}".format(self.n_bids))
        formula.header.append(" # Values: {0}".format(self.n_goods))
        formula.header.append(" # Dummies: {0}".format(self.n_dummies))
        formula.header.append("--------------------")
        formula.header.append("")
