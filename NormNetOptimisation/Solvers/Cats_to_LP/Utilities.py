import random
import PARAMETERS


def getNonGeneralised(P, NN):
    # Same name as in the paper
    # Gives you the subset of norms not generalised by any other norm

    G = NN.copy()
    for i in NN:
        for j in NN[i]:
            if j in G:
                del G[j]

    return G


def getSiblingGroups(P, NN):
    #  Same name as in the paper
    #  Returns  the groups of siblings of norms that are not in P

    sibList = {}

    for i in NN:
        if i not in P:
            sibList[i] = NN[i]

    return sibList


def SUPERADDITIVE_GENERALISATION_POWER(NN):
    # Same name as the paper  Algorithm 1
    # The algorithm does exactly the same

    N = {}
    for i in NN:
        N[i] = 0

    g = {}
    P = N.copy()
    G = getNonGeneralised(P, NN)

    for i in G:
        g[i] = 1
        del P[i]

    while P:
        sibList = getSiblingGroups(P, NN)

        for ps in sibList:
            for i in sibList[ps]:
                epsilon = random.randrange(0,1)*PARAMETERS.MAX_EPSILON
                g[i] = (float(g[ps])*(1-epsilon)) / len(sibList[ps])
                if i in P:
                    del P[i]

    return g


def recurIncl(NN, i):
    # This is the method described in the section 3.2.1
    # A recursive method to find the inclusion power

    if len(NN[i]) == 0:
        return 1
    else:
        r = 1

        for j in NN[i]:
            r = r + recurIncl(NN, j)

        return r


def INCLUSIONCOMP(NN):
    # Applies the previous method to each norm we have

    r = {}

    for i in NN:
        r[i] = recurIncl(NN, i)

    return r


def recAnc(NN, j):
    # I thought this method myself. It simply mimicks the one to calculate the inclusion power
    # It also is recursive
    # You get the ancestors of a norm j

    r = [j]

    for i in NN:
        if len(NN[i]) > 0:
            if j in NN[i]:
                r = r + recAnc(NN, i)

    return r


def ANCESTORS(NN):
    # Applies the previous method to each norm we have

    a = {}

    for i in NN:
        a[i] = recAnc(NN, i)
        del a[i][0]
        a[i].sort()

        if len(a[i]) > 0:
            for j in a[i]:
                if i in NN[j]:
                    a[i].remove(j)

    return a


def get_prices(root_costs,NN):
    # Calculates the cost of each child based on the cost of the father

    costs = {}
    toExplore = []
    for r in root_costs.keys():
        costs[r] = root_costs[r]
        toExplore.append(r)

    while toExplore:
        norm = toExplore.pop(0)
        for child in NN[norm]:
            costs[child] = costs[norm]*0.5
            toExplore.append(child)

    return costs

def get_budget(cost, roots, percent):
    """
    It calculates the best case: having money to pay every root norm. Then applies the user
    choosed percentatge
    :param cost:
    :param roots:
    :param unit:
    :return:
    """

    b = 0

    for r in roots:
        b +=cost[r]

    return b*percent



