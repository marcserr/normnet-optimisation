import CATSAuction

# With this program you can create an LP from a CATS file.
# The difference with simple.py and cats2cplex.py is that here you don't solve it
def main():
    cats_file = 'loquesea.txt'
    lp_file = 'loquequieras.txt'


    problem_type= 'MNSPLB'
    p_generalization = 0.50
    representativity = 2
    width = 2
    sigma_g = 0
    sigma_h = 0
    sigma_w = 0
    repower = 'I'
    budget = 0.50






    auction = CATSAuction.CATSAuction()
    auction.load_file(cats_file)
    formula = auction.to_LP(False,False)
    formula.write_LP_file(lp_file, problem_type,repower, p_generalization, sigma_g, representativity, sigma_h, width, sigma_w, budget)

if __name__ == "__main__":
    main()
