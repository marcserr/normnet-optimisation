import time
import cplex

def problem(problem_lp, problem_sol, timeLim):

    #Receives a LP file and solves it. It exports the answer to a .sol file and also shows it.
    #You can specify the time limit with timeLim.

    try:


        m=cplex.Cplex(problem_lp)

        m.parameters.timelimit.set(timeLim)
        m.parameters.mip.limits.treememory.set(2800)
        m.parameters.threads.set(1)
        start_time = time.time()
        m.solve()
        final_time = time.time()-start_time

        m.solution.write(problem_sol)


        f = open(problem_sol, 'w')
        f.write(100*'_')
        f.write('\n')
        f.write('Solved in ' + str(final_time) + ' seconds.')

        for varName, varValue in enumerate(m.solution.get_values()):
            if varValue!=0:
                f.write(str(varName)+' = '+str(varValue))

        f.close()

    except cplex.exceptions.CplexError:
        print 'Sorry'


