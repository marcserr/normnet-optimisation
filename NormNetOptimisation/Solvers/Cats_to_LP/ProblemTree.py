from random import *
import random
import networkx as nx

import PARAMETERS


class ProblemTree:

    def __init__(self, G):
        self.G = G

    def __len__(self):
        return len(self.G)

    def get_list(self):
        # a partir de un grafo de tipo arbol, crea una lista de cada nodo y sus hijos


        total = {}

        for node in self.G.nodes():
            w = list(self.G.neighbors(node))
            w.sort()

            total[node] = []
            for object in w:
                if node < object:
                    total[node] = total[node] + [object]

        return total


    def tree_killer(self, x):
        # Elimina al azar x nodos del arbol. Se asegura de que no quede ningun nodo suelto

        killed = []
        for i in range(x):
            y = randint(1, len(self.G) - 1)
            while y in killed:
                y = randint(1, len(self.G) - 1)
            killed.append(y)
            self.G.remove_node(y)

        for i in self.G.nodes():
            if not nx.has_path(self.G, 0, i):
                self.G.remove_node(i)


    def tree_height_reviver(self, height_min):
        # Se asegura que el arbol cumple el minimo de altura

        self.tree_reorganiser()
        max_height = 0
        for node in self.G.nodes():
            length = nx.shortest_path_length(self.G, 0, node)
            if max_height < length:
                max_height = length

        difference = height_min - max_height

        while difference > 0:
            self.G.add_edge(len(self.G) - 1, len(self.G))
            difference = difference - 1


    def tree_range_reviver(self, range_min):
        # Se asegura que el arbol cumple el minimo de anchura
        self.tree_reorganiser()
        fills = {}

        for i in self.G.nodes():
            fills[i] = 0
            for neighbor in self.G.neighbors(i):
                if neighbor > i:
                    fills[i] = fills[i] + 1

        for i in fills:
            if fills[i] > 0:
                while fills[i] < range_min:
                    self.G.add_edge(i, len(self.G))
                    fills[i] = fills[i] + 1


    def tree_reviver(self, height_min, range_min):
        # Se asegura de que el arbol creado cumpla los minimos poniendo nodos extra

        # Nos aseguramos de la altura minima
        self.tree_height_reviver(height_min)

        # Y por ultimo de la anchura minima

        self.tree_range_reviver(range_min)


    def tree_reorganiser(self):
        # Pone los nodos del arbol reordenados
        # Si teniamos  [1,2,4] lo convierte en [1,2,3]
        map = {}
        int = 0

        for i in self.G:
            map[i] = int
            int = int + 1

        return nx.relabel_nodes(self.G, map)

    def getG(self):
        return self.G

    @staticmethod
    def random_tree(range_min, range_max, height_min, height_max, cuts):
        # crea un grafo de tipo arbol al azar siguiendo los minimos y los maximos asignados

        # Primero crea el maximo arbol posible

        tree = nx.balanced_tree(range_max, height_max)
        G = ProblemTree(tree)
        # Luego le cortamos algunas hojas
        if cuts:
            if len(G)/4 <=1:
                x = 1
            else:
                x = randint(1, len(G) / 4)

            G.tree_killer(x)
        # Y finalmente nos aseguramos de que no nos hemos pasado cortando

        G.tree_reviver(height_min, range_min)

        return G.tree_reorganiser()

    @staticmethod
    def get_g(n_arboles, representativity, sigma_h, width, sigma_w):
        # Crea una lista con todos los arboles

        h = representativity
        g = {}

        for i in range(n_arboles):

            r1 = int(abs(gauss(width, sigma_w)))
            if r1 < 1: r1 = 1

            r2 = int(abs(gauss(width, sigma_w)))
            if r2 < 1: r2 = 1

            if r1 < r2:
                range_min = r1
                range_max = r2
            elif r2 < r1:
                range_min = r2
                range_max = r1
            else:
                range_min = r1
                range_max = r1

            r1 = int(abs(gauss(h, sigma_h)))
            if r1 < 1: r1 = 1

            r2 = int(abs(gauss(h, sigma_h)))
            if r2 < 1: r2 = 1

            if r1 < r2:
                height_min = r1
                height_max = r2
            elif r2 < r1:
                height_min = r2
                height_max = r1
            else:
                height_min = r1
                height_max = r1

            g[i] = ProblemTree.random_tree(range_min, range_max, height_min, height_max, PARAMETERS.CUTS)

        return g

    @staticmethod
    def makeSingleTree(height, sigma_h, width, sigma_w):
        r1 = int(abs(gauss(width, sigma_w)))
        if r1 < 1: r1 = 1

        r2 = int(abs(gauss(width, sigma_w)))
        if r2 < 1: r2 = 1

        if r1 < r2:
            range_min = r1
            range_max = r2
        elif r2 < r1:
            range_min = r2
            range_max = r1
        else:
            range_min = r1
            range_max = r1

        r1 = int(abs(gauss(height, sigma_h)))
        if r1 < 1: r1 = 1

        r2 = int(abs(gauss(height, sigma_h)))
        if r2 < 1: r2 = 1

        if r1 < r2:
            height_min = r1
            height_max = r2
        elif r2 < r1:
            height_min = r2
            height_max = r1
        else:
            height_min = r1
            height_max = r1

        return ProblemTree.random_tree(range_min, range_max, height_min, height_max, PARAMETERS.CUTS)

    @staticmethod
    def get_NN(roots, p_generalization, sigma_g, height, sigma_h, width, sigma_w):
        # Crea un grafo con todos los arboles que hemos creado (gu)

        """
        n_solitarios = int(abs(gauss(roots * (1 - p_generalization), sigma_g)))

        n_arboles = roots - n_solitarios

        g = ProblemTree.get_g(n_arboles, height, sigma_h, width, sigma_w)

        gu = g[0]

        for i in range(1, n_arboles):
            gu = nx.disjoint_union(gu, g[i])

        uni = nx.Graph()
        uni.add_node(1)

        for i in range(n_solitarios):
            gu = ProblemTree.disjoint_union(gu, uni)

            # descomentar para ver los dibujitos
            # nx.draw(gu,with_labels=True)
            # plt.show()

        if isinstance(gu, nx.Graph):
            gu = ProblemTree(gu)

        return gu.get_list()
        """
        trees = 0
        prob_of_tree = int(abs(gauss(int(p_generalization*100), sigma_g)))
        arbitrary_number = random.randrange(1,101,1)
        root_with_tree =  arbitrary_number <= prob_of_tree
        if PARAMETERS.BUILD_PROBLEM_TYPE == "VMNSPLB":
            problem_graph = nx.Graph()
            problem_graph.add_node(0)
            for i in range(roots):
                problem_graph.add_node(i)

            problem_graph = ProblemTree(problem_graph)
        else:
            if root_with_tree and not height == 0:
                problem_graph = ProblemTree.makeSingleTree(height, sigma_h, width, sigma_w)
            else:
                problem_graph = nx.Graph()
                problem_graph.add_node(1)

            for i in range(roots-1):
                arbitrary_number = random.randrange(1,101,1)
                root_with_tree = arbitrary_number <= prob_of_tree
                if root_with_tree and not height == 0:
                    trees += 1
                    new_root = ProblemTree.makeSingleTree(height, sigma_h, width, sigma_w)
                else:
                    new_root = nx.Graph()
                    new_root.add_node(1)

                problem_graph = ProblemTree.disjoint_union(problem_graph, new_root)

        return problem_graph.get_list(), trees


    # Creamos NN. Estos son todos los nodos y sus respectivos hijos

    @staticmethod
    def disjoint_union(PT1, PT2):
        g1 = PT1
        g2 = PT2
        if isinstance(PT1, ProblemTree):
            g1 = PT1.getG()
        if isinstance(PT2, ProblemTree):
            g2 = PT2.getG()
        return ProblemTree(nx.disjoint_union(g1, g2))

    @staticmethod
    def get_lengths(NN):
        # Devuelve una lista indicando el numero de los nodos raiz.
        # Si tenemos dos arboles de 6 nodos cada uno, te devolvera la lista [0,6] por ejemplo

        lengths = [i for i in NN]

        for node in NN:
            for fill in NN[node]:
                if fill in lengths:
                    lengths.remove(fill)

        return lengths

