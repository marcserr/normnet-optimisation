#!/usr/bin/env python
# -*- coding: utf -*-

import itertools
import sys
import os
import random
import Utilities
from ProblemTree import ProblemTree

import PARAMETERS

class LPException(Exception):
    """Invalid CPLEX operation."""


class LPFormula(object):

    def __init__(self):
        self.num_vars = 0
        self.hard = []  # Item format: [literals]
        self.soft = []  # Item format: (weight, [literals])
        self.values = {} #Item format: (norm index, utility value)
        self._sum_soft_weights = 0
        self.header = []
        self.NN = None
        self.roots = None
        self.exclusivity = []

    @property
    def num_clauses(self):
        return len(self.hard) + len(self.soft)

    @property
    def top_weight(self):
        return self._sum_soft_weights + 1

    def clean(self):
        self.__init__()

    def add_clauses(self, clauses, weight=PARAMETERS.TOP_WEIGHT):
        """Adds the given set of clauses, having each one the specified weight.

        :param clauses: Iterable filled with sets of literals.
        :type clauses: list[list[int]]
        :param weight: Weight applied to all the clauses, as in add_clause().
        :type weight: int
        """
        for c in clauses:
            self.add_clause(c, weight)

    def saveValues(self, index, values, vals_file = None):
        self.values[index] = values
        if vals_file:
            vals_file.write(str(index)+": "+str(list(self.values[index]))+"\n")

    def buildNormUtilities(self, value_utilities):
        self.buildRoots()
        currentRootPos = 0
        v= []
        nextRoot = self.roots[0]
        currentRootUtility = None
        for i in range(len(self.NN)):
            if i == nextRoot:
                currentRootUtility = self.utilityValue(currentRootPos, value_utilities)
                currentRootPos +=1
                if currentRootPos < len(self.roots):
                    nextRoot = self.roots[currentRootPos]
            v.append(currentRootUtility)

        if PARAMETERS.NORMALIZE_VALUE_UTILITIES:
            maximum = max(v)
            minimum = min(v)
            for i in range(len(v)):
                if not v[i]:
                    print i
            for i in range(len(v)):
                v[i] = (v[i]-minimum)/(maximum-minimum)
        return v


    def utilityValue(self, node, value_utilities):
        u = 0
        for value in self.values[node]:
            if PARAMETERS.RANDOM_VALUE_SUPPORT:
                support = (random.random()-0.5)*2.
            else:
                support = 1.
            u += support * value_utilities[value]
        return u

    def add_clause(self, literals, weight):
        """Adds the given literals as a new clause with the specified weight.

        :param literals: Clause literals
        :type literals: list[int]
        :param weight: Clause weight, less than 1 means infinity.
        :type weight: int
        """
        self._check_literals(literals)
        self._add_clause(literals, weight)

    def add_exactly_one(self, literals, weight):
        """Adds the necessary combination of clauses to ensure that exactly
        one of the given literals evaluates to true.

        :param literals: Literals to include in the exactly one set of clauses.
        :type literals: list[int]
        :param weight: Clauses weight, less than 1 means infinity.
        :type weight: int
        """
        self._check_literals(literals)
        self._add_at_least_one(literals, weight)
        self._add_at_most_one(literals, weight)

    def add_at_least_one(self, literals, weight):
        """Adds the necessary combination of clauses to ensure that at least
        one of the given literals evaluates to true.

        :param literals: Literals to include in the at most one set of clauses.
        :type literals: list[int]
        :param weight: Clause weight, less than 1 means infinity.
        :type weight: int
        """
        self._check_literals(literals)
        #self._add_at_least_one(literals, weight)

    def add_at_most_one(self, literals, weight):
        """Adds the necessary combination of clauses to ensure that at most
        one of the given literals evaluates to true.

        :param literals: Literals to include in the at most one set of clauses.
        :type literals: list[int]
        :param weight: Clauses weight, less than 1 means infinity.
        :type weight: int
        """
        self._check_literals(literals)
        self._add_at_most_one(literals, weight)

    def new_var(self):
        """Returns the next free variable of this formula.

        :return: The next free variable (>1).
        :rtype: int
        """
        self.num_vars += 1
        return self.num_vars

    def extend_vars(self, how_many):
        """Extends the number of used variables.
        """
        if how_many < 0:
            raise ValueError("Cannot be extended a negative quantity")
        self.num_vars += how_many

    def MNSP(self, stream,repower,  p_generalization, sigma_g, representativity, sigma_h, width, sigma_w):

        self.NN = ProblemTree.get_NN(len(self.soft),p_generalization, sigma_g, representativity, sigma_h, width, sigma_w) #manel.tree

        if isinstance(stream, str):
            stream = open(stream, "w")

        stream.write('/* # Norms: ' + str(len(self.NN))+ ' */\n')
        stream.write("/*--------------------*/\n")

        stream.write("Maximize\n")


        if repower == 'I':
            r = Utilities.INCLUSIONCOMP(self.NN)
        else:
            r = Utilities.SUPERADDITIVE_GENERALISATION_POWER(self.NN)

        i = 0
        for norm in self.NN:
            if i == 0:
                i = 1
            else:
                stream.write(' + ')
            stream.write('{0:.8f}'.format(r[norm]) + ' n_' + str(norm))
        stream.write('\n')

        f = ProblemTree.get_lengths(self.NN) #manel.tree
        stream.write("Subject To\n")
        for c in self.hard:
            stream.write('n_' + str(f[c[0] - 1]) + ' + n_' + str(f[c[1] - 1]) + ' <= 1\n')

        # Const 2
        for norm in self.NN:
            for child in self.NN[norm]:
                stream.write('n_' + str(norm) + ' + n_' + str(child) + ' <= 1\n')

        # Const 3

        for norm in self.NN:
            i = 0
            if self.NN[norm]:
                for child in self.NN[norm]:
                    if i == 0:
                        i = 1
                    else:
                        stream.write(' + ')
                    stream.write('n_' + str(child))
                stream.write(' <= ' + str(len(self.NN[norm]) - 1)+'\n')
        # Const 4


        # Now we calculate the ancestors
        A = Utilities.ANCESTORS(self.NN)

        for norm in self.NN:
            for ancestor in A[norm]:
                stream.write('n_' + str(norm) + ' + n_' + str(ancestor) + ' <= 1\n')

        stream.write('Binaries\n')
        for norm in self.NN:
            stream.write('n_'+str(norm)+'\n')

        stream.close()

    def MNSPLB(self, repowers, p_generalization, sigma_g, height, budget, sigma_h, width, sigma_w, num_exc_rels, build_round, file_paths = None, stream = sys.stdout):

        lp_files = []
        if file_paths:
            for f_path in file_paths:
                file_to_append = open(f_path, "a")
                lp_files.append(file_to_append)
        else:
            lp_files = [stream]

        self.NN, trees = ProblemTree.get_NN(len(self.soft), p_generalization, sigma_g, height, sigma_h, width, sigma_w)
        roots = ProblemTree.get_lengths(self.NN)
        self.translateHard(roots)

        if PARAMETERS.EXCLUSIVITY_PROPAGATION:
            sum1 = self.PropagateExclusivityDown()
            sum2 = self.PropagateExclusivityUp()
            minus1 = self.checkPropagation()
            num_exc_rels = num_exc_rels+sum1+sum2-minus1


        roots = ProblemTree.get_lengths(self.NN)
        self.translateHard(roots)

        if PARAMETERS.EXCLUSIVITY_PROPAGATION:
            sumEx = self.PropagateExclusivity()
            num_exc_rels = num_exc_rels + sumEx

        if PARAMETERS.SAVE_STRUCTURE:
            data = file_paths[0].split("/")
            file_num_norms = data[-2].split("N")[0]
            file_num_goods = data[-2].split("N")[1].replace("G", "")
            data2 = data[-1].split("_")
            file_height = data2[2]
            file_width = data2[3]
            file_gen = data2[4].replace("g", "")
            file_budget = data2[5].replace("b.lp", "")
            self.saveStructure(file_num_norms, file_num_goods, file_height, file_width, file_gen, file_budget, build_round, "G")
        num_gen_rel = 0

        for i in range(1,int(height)+1):
            num_gen_rel = i*trees*int(width)**i

        for lp_file in lp_files:
            lp_file.write('/* # Norms: ' + str(len(self.NN))+ ' */\n')
            lp_file.write('/* # Generalisation relations: ' + str(num_gen_rel)+ ' */\n')
            lp_file.write('/* # Exclusivity relations: ' + str(num_exc_rels)+ ' */\n')
            lp_file.write('/* # Roots: '+str(roots)+ ' */\n')
            lp_file.write("/*--------------------*/\n")
            lp_file.write("Maximize\n")

        r = []

        for i in range(len(repowers)):
            repower = repowers[i]
            if repower == 'I':
                r.append(Utilities.INCLUSIONCOMP(self.NN))
            else:
                r.append(Utilities.SUPERADDITIVE_GENERALISATION_POWER(self.NN))

        for i in range(len(lp_files)):
            lp_file = lp_files[i]
            root_costs = {}

            for w,n in self.soft:
                root_costs[roots[n[0]-1]] = w

            b = sum(root_costs.values())*budget

            costs = Utilities.get_prices(root_costs,self.NN)

            N = {}
            for node in self.NN:
                N[node] = 0
            P = N.copy()
            G = Utilities.getNonGeneralised(P, self.NN)

            R_max = 0

            for j in G:
                R_max = R_max + r[i][j]

            w_r = PARAMETERS.REPCOSTVALUE_WEIGHT[0]
            w_c = PARAMETERS.REPCOSTVALUE_WEIGHT[1]

            firstElem = True
            for norm in self.NN:
                if firstElem:
                    firstElem = False
                else:
                    lp_file.write(' + ')
                lp_file.write('{0:.8f}'.format(float(w_r*r[i][norm]) / R_max )+ ' n_' + str(norm) + ' - ' + '{0:.8f}'.format(float(w_c*costs[norm]) / b ) + ' n_' + str(norm))
            lp_file.write(' + '+str(w_c)+' y \n')


            lp_file.write("Subject To\n")

            #Hard Constraints: Exclusivity relations
            for x in self.hard:
                lp_file.write('n_' + str(x[0]) + ' + n_' + str(x[1]) + ' <= 1\n')

            # Const 2: A norm and it's child cannot be selected
            for norm in self.NN:
                for child in self.NN[norm]:
                    lp_file.write('n_' + str(norm) + ' + n_' + str(child) + ' <= 1\n')

            # Const 3: All childs of a tree cannot be selected at once
            for norm in self.NN:
                i = 0
                if self.NN[norm]:
                    for child in self.NN[norm]:
                        if i == 0:
                            i = 1
                        else:
                            lp_file.write(' + ')
                        lp_file.write('n_' + str(child))
                    lp_file.write(' <= ' + str(len(self.NN[norm])-1)+'\n')



            # Now we calculate the ancestors
            A = Utilities.ANCESTORS(self.NN)

            # Const 4: A norm and an ancestor cannot be selected at the same time
            for norm in self.NN:
                for ancestor in A[norm]:
                    lp_file.write('n_' + str(norm) + ' + n_' + str(ancestor) + ' <= 1\n')



            # Const 9: The selected norms cannot exceed the cost of the budget
            i=0
            for norm in self.NN:
                if i==0:
                    i+=1
                else:
                    lp_file.write(' + ')
                lp_file.write('{0:8f}'.format(costs[norm])+ ' n_' +str(norm))
            lp_file.write(' <= '+ str(b)+'\n')

            # Const 12:
            i = 0
            for norm in self.NN:
                if i == 0:
                    i += 1
                else:
                    lp_file.write(' + ')
                lp_file.write(' n_' + str(norm))
            lp_file.write(' - y >= ' + '0\n')

            i = 0
            for norm in self.NN:
                if i == 0:
                    i += 1
                else:
                    lp_file.write(' + ')
                lp_file.write(' n_' + str(norm))
            lp_file.write(' '+str(-len(self.NN)-1)+' y <= 0\n')

            lp_file.write('Binaries\n')
            for norm in self.NN:
                lp_file.write('n_'+str(norm)+'\n')
            lp_file.write('y\n')

            lp_file.write('End\n')

        for lp_file in lp_files:
            lp_file.close()

    def VMNSPLB(self, repowers, p_generalization, sigma_g, height, budget, sigma_h, width, sigma_w, num_exc_rels, build_round, file_paths = None, stream = sys.stdout, preferences = None):

        lp_files = []
        if file_paths:
            for f_path in file_paths:
                file_to_append = open(f_path, "a")
                lp_files.append(file_to_append)
        else:
            lp_files = [stream]

        self.NN, trees = ProblemTree.get_NN(len(self.soft), p_generalization, sigma_g, height, sigma_h, width, sigma_w)

        if PARAMETERS.SAVE_STRUCTURE:
            data = file_paths[0].split("/")
            file_num_norms = data[-2].split("N")[0]
            file_num_goods = data[-2].split("N")[1].replace("V", "")
            data2 = data[-1].split("_")
            file_height = data2[2]
            file_width = data2[3]
            file_gen = data2[4].replace("g", "")
            file_budget = data2[5].replace("b.lp", "")
            self.saveStructure(file_num_norms, file_num_goods, file_height, file_width, file_gen, file_budget, build_round, "V")
        num_gen_rel = 0

        for i in range(1,int(height)+1):
            num_gen_rel = i*trees*int(width)**i

        for lp_file in lp_files:
            lp_file.write('/* # Norms: ' + str(len(self.NN))+ ' */\n')
            lp_file.write('/* # Generalisation relations: ' + str(num_gen_rel)+ ' */\n')
            lp_file.write('/* # Exclusivity relations: ' + str(num_exc_rels)+ ' */\n')
            lp_file.write("/*--------------------*/\n")
            lp_file.write("Maximize\n")

        r = []

        for i in range(len(repowers)):
            repower = repowers[i]
            if repower == 'I':
                r.append(Utilities.INCLUSIONCOMP(self.NN))
            else:
                #Other possible representation power functions should go here
                pass

        v = self.buildNormUtilities(preferences)

        for i in range(len(lp_files)):
            lp_file = lp_files[i]
            roots = ProblemTree.get_lengths(self.NN)
            root_costs = {}

            for w,n in self.soft:
                root_costs[roots[n[0]-1]] = w

            b = sum(root_costs.values())*budget

            costs = Utilities.get_prices(root_costs,self.NN)

            N = {}
            for node in self.NN:
                N[node] = 0
            P = N.copy()
            G = Utilities.getNonGeneralised(P, self.NN)

            R_max = 0

            for j in G:
                R_max = R_max + r[i][j]

            V_max = 0
            for norm in self.NN:
                if v[norm] > 0:
                    V_max += v[norm]

            w_r = PARAMETERS.REPCOSTVALUE_WEIGHT[0]
            w_c = PARAMETERS.REPCOSTVALUE_WEIGHT[1]
            w_v = PARAMETERS.REPCOSTVALUE_WEIGHT[2]

            firstElem = True
            for norm in self.NN:
                if firstElem:
                    firstElem = False
                else:
                    lp_file.write(' + ')
                lp_file.write('{0:.8f}'.format(float(w_r*r[i][norm]) / R_max )+ ' n_' + str(norm) + ' - ' + '{0:.8f}'.format(float(w_c*costs[norm]) / b ) + ' n_' + str(norm) + ' + '+'{0:.8f}'.format(float(w_v*v[norm]) / V_max )+ ' n_' + str(norm) )
            lp_file.write(' + '+str(w_c)+' y \n')


            lp_file.write("Subject To\n")

            #Hard Constraints: Exclusivity relations
            for x in self.hard:
                lp_file.write('n_' + str(roots[x[0] - 1]) + ' + n_' + str(roots[x[1] - 1]) + ' <= 1\n')

            # Const 2: A norm and it's child cannot be selected
            for norm in self.NN:
                for child in self.NN[norm]:
                    lp_file.write('n_' + str(norm) + ' + n_' + str(child) + ' <= 1\n')

            # Const 3: All childs of a tree cannot be selected at once
            for norm in self.NN:
                i = 0
                if self.NN[norm]:
                    for child in self.NN[norm]:
                        if i == 0:
                            i = 1
                        else:
                            lp_file.write(' + ')
                        lp_file.write('n_' + str(child))
                    lp_file.write(' <= ' + str(len(self.NN[norm])-1)+'\n')



            # Now we calculate the ancestors
            A = Utilities.ANCESTORS(self.NN)

            # Const 4: A norm and an ancestor cannot be selected at the same time
            for norm in self.NN:
                for ancestor in A[norm]:
                    lp_file.write('n_' + str(norm) + ' + n_' + str(ancestor) + ' <= 1\n')



            # Const 9: The selected norms cannot exceed the cost of the budget
            i=0
            for norm in self.NN:
                if i==0:
                    i+=1
                else:
                    lp_file.write(' + ')
                lp_file.write('{0:8f}'.format(costs[norm])+ ' n_' +str(norm))
            lp_file.write(' <= '+ str(b)+'\n')

            # Const 12:
            i = 0
            for norm in self.NN:
                if i == 0:
                    i += 1
                else:
                    lp_file.write(' + ')
                lp_file.write(' n_' + str(norm))
            lp_file.write(' - y >= ' + '0\n')

            i = 0
            for norm in self.NN:
                if i == 0:
                    i += 1
                else:
                    lp_file.write(' + ')
                lp_file.write(' n_' + str(norm))
            lp_file.write(' '+str(-len(self.NN)-1)+' y <= 0\n')

            lp_file.write('Binaries\n')
            for norm in self.NN:
                lp_file.write('n_'+str(norm)+'\n')
            lp_file.write('y\n')

            lp_file.write('End\n')

        for lp_file in lp_files:
            lp_file.close()


    def saveStructure(self, num_norms, num_goods, height, width, gen, budget, round, gorv):
        structDirectory = os.getcwd().replace("Solvers", "Graphics").replace("Cats_to_LP", "")+"/"+num_norms+"N"+num_goods+gorv+"/STRUCT/"
        structFileName = structDirectory+"STRUCT_"+str(height)+"_"+str(width)+"_"+str(gen)+"g_"+str(budget)+"b_"+str(round)+".txt"
        structFile = open(structFileName, "w")
        structFile.write("Graph structure\n")
        roots = range(len(self.NN))
        for node in self.NN:
            if self.NN[node]:
                for child in self.NN[node]:
                    roots.remove(child)
        currentNode = 0
        towrite = []
        for r in roots:
            towrite.append((currentNode, r))
            currentNode += 1
        while towrite:
            node, NNnode = towrite.pop(0)
            lineToPrint = str(node)+":"
            if self.NN[NNnode]:
                for child in self.NN[NNnode]:
                    lineToPrint+= str(currentNode)+","
                    towrite.append((currentNode, child))
                    currentNode += 1
                lineToPrint = lineToPrint[:-1]
            structFile.write(lineToPrint+'\n')
        structFile.write("Goods ("+str(num_goods)+")\n")
        CATSDirectory = structDirectory.replace("STRUCT", "CATS")
        CATSFileName = CATSDirectory+"CATS_"+str(num_norms)+"N_"+str(height)+"_"+str(width)+"_"+str(gen)+"_"+str(round)+".txt"
        CATSFile = open(CATSFileName, "r")
        look = False
        for line in CATSFile:
            if line[0] == "0":
                look = True
            if look:
                data = line.split()
                node = data[0]
                towrite = node+":"
                if data[2:]:
                    for i in data[2:]:
                        if i.isdigit():
                            if int(i) < int(num_goods):
                                towrite += i + ","
                    towrite = towrite[:-1]
                structFile.write(towrite+'\n')


    def write_LP(self, problem, repowers, p_generalization, sigma_g, representativity, sigma_h, width, sigma_w, budget, build_round, num_exc_rels = None, file_paths = None, stream=sys.stdout, preferences = None):
        """Writes the formula in LP format into the specified stream.

        :param stream: A writable stream object.
        """

        if file_paths:
            for file in file_paths:
                f = open(file, "w")
                for line in self.header:
                    f.write("/* "+line+' */\n')
                f.close()
        else:
            for line in self.header:
                stream.write("/* "+line+' */\n')

        if problem == 'MNSP':
            self.MNSP(stream,repowers, p_generalization, sigma_g, representativity, sigma_h, width, sigma_w)
        elif problem == 'MNSPLB':
            if file_paths:
                self.MNSPLB(repowers, p_generalization, sigma_g, representativity, budget, sigma_h, width, sigma_w, num_exc_rels, build_round, file_paths = file_paths)
            else:
                self.MNSPLB(repowers, p_generalization, sigma_g, representativity, budget, sigma_h, width, num_exc_rels, sigma_w, build_round, stream = stream)
        elif problem == 'VMNSPLB':
            if file_paths:
                self.VMNSPLB(repowers, p_generalization, sigma_g, representativity, budget, sigma_h, width, sigma_w, num_exc_rels, build_round, file_paths = file_paths, preferences = preferences)
            else:
                self.VMNSPLB(repowers, p_generalization, sigma_g, representativity, budget, sigma_h, width, num_exc_rels, sigma_w, build_round, stream = stream, preferences = preferences)
        else:
            print "INVALID PROBLEM TYPE"

    def write_LP_file(self, file_path, problem,repowers, p_generalization, sigma_g, representativity, sigma_h, width, sigma_w, budget):
        """Writes the formula in DIMACS format into the specified file.

        :param file_path: Path to a writable file.
        :type file_path: str
        """
        f = open(file_path, 'w')
        self.write_LP(problem,repowers,p_generalization, sigma_g, representativity, sigma_h, width, sigma_w, budget, stream = f)
        f.close()

    def _add_clause(self, literals, weight):
        if weight < 1:
            self.hard.append(literals)
        else:
            self.soft.append((weight, literals))
            self._sum_soft_weights += weight

    def _add_at_least_one(self, literals, weight):

        self.add_clause(literals, weight)

    def _add_at_most_one(self, literals, weight):
        for l1, l2 in itertools.combinations(literals, 2):
            self.add_clause([-l1, -l2], weight)
        # Regular encoding
        # n = len(literals)
        # if n > 1:
        #     aux = [self.new_var() for _ in range(1, n)]
        #
        #     # x_1 -> not r_2
        #     self.add_clause(weight, [-literals[0], -aux[0]])
        #     # x_i -> r_i or not r_i + 1 | i > 2 & i < n
        #     for i in range(1, n - 1):
        #         self.add_clause(weight, [-literals[i], aux[i - 1]])
        #         self.add_clause(weight, [-literals[i], -aux[i]])
        #     # x_n -> r_n
        #     if n > 1:
        #         self.add_clause(weight, [-literals[n - 1], aux[n - 2]])
        #     # r_i+1 -> r_i
        #     for i in range(len(aux) - 1, 1, -1):
        #         self.add_clause(weight, [-aux[i], aux[i - 1]])

    def _check_literals(self, literals):
        for var in map(abs, literals):
            if var == 0:
                raise LPException("Clause cannot contain variable 0")
            elif self.num_vars < var:
                raise LPException("Clause contains variable {0}, not defined"
                                    " by new_var()".format(var))

    def buildRoots(self):
        if not self.roots and self.NN:
            self.roots = ProblemTree.get_lengths(self.NN)
        return self.roots

    def PropagateExclusivity(self):
        num = 0
        for ex in self.hard:
            n1 = ex[0]
            n2 = ex[1]
            toPropagate = self.NN[n1][:]
            while toPropagate:
                n = toPropagate.pop(0)
                if n < n2 and not [n,n2] in self.hard:
                    self.hard.append([n, n2])
                    num += 1
                elif [n2,n] not in self.hard:
                    self.hard.append([n2, n])
                    num += 1
                toPropagate += self.NN[n][:]
            toPropagate = self.NN[n2][:]
            while toPropagate:
                n = toPropagate.pop(0)
                if n < n1:
                    self.hard.append([n, n1])
                    num += 1
                else:
                    self.hard.append([n1, n])
                    num += 1
                toPropagate += self.NN[n][:]
        return num


    def translateHard(self, roots):
        newHard = []
        for pair in self.hard:
            newHard.append([roots[pair[0]-1],roots[pair[1]-1]])
        self.hard = newHard

    def __str__(self):
        ss = os.io.StringIO()
        self.write_LP(stream=ss)
        output = ss.getvalue()
        ss.close()
        return output

    def PropagateExclusivityDown(self):
        num = 0
        for ex in self.hard:
            n1 = ex[0]
            n2 = ex[1]
            toPropagate = self.NN[n1][:]
            while toPropagate:
                n = toPropagate.pop(0)
                if n < n2:
                    self.hard.append([n, n2])
                    num += 1
                else:
                    self.hard.append([n2, n])
                    num += 1
                toPropagate += self.NN[n][:]
            toPropagate = self.NN[n2][:]
            while toPropagate:
                n = toPropagate.pop(0)
                if n < n1:
                    self.hard.append([n, n1])
                    num += 1
                else:
                    self.hard.append([n1, n])
                    num += 1
                toPropagate += self.NN[n][:]
        return num

    def PropagateExclusivityUp(self):
        #TODO: To end not necessary for tests
        return 0

    def checkPropagation(self):
        toDel = []
        for i in range(len(self.hard)):
            for j in range(i+1, len(self.hard)):
                p1 = self.hard[i]
                p2 = self.hard[j]
                if p1[0] == p2[0] and p1[1] == p2[1]:
                    toDel.append(j)
                elif p1[0] == p2[1] and p1[1] == p2[0]:
                    toDel.append(j)
        newHard = []
        for i in range(len(self.hard)):
            if i not in toDel:
                newHard.append(self.hard[i])
        num = len(self.hard)-len(newHard)
        self.hard = newHard
        return num

    def translateHard(self, roots):
        newHard = []
        for pair in self.hard:
            newHard.append([roots[pair[0]-1],roots[pair[1]-1]])
        self.hard = newHard


