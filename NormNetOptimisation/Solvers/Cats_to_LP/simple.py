# A simplified version of manel_ca2cplex.py

import CATSAuction
import CPLEXSolver

def w(word):

    if word=='low':
        return 0.25
    elif word=='medium':
        return 0.5
    elif word=='high':
        return 0.75
    else:
        return 1.0
def main():
    auction = CATSAuction.CATSAuction()

    cats_file = 'instance1.txt'
    lp_file = 'dimacs.lp'
    result_file = 'result.txt'
    time_allowed = 10

    auction.load_file(cats_file)
    print ""
    print "Auction loaded"
    print ""

    formula = auction.to_LP(False,False)


    # low = 0.25, medium = 0.5, high = 0.75, maximum = 1.0

    # 1er parametro

    problem_type= 'MNSPLB'

    # 2o

    repower = 'G'

    # 3o

    p_generalization =0.25

    # 4o

    sigma_g = 0.2

    # 5o

    height = 3

    # 6o

    sigma_h = 0.2

    # 7o

    width = 1

    # 8o

    sigma_w = 0.3

    # 9o

    budget = w('high')


    formula.write_LP_file(lp_file, problem_type,repower, p_generalization, sigma_g, height, sigma_h, width, sigma_w, budget)

    CPLEXSolver.problem(lp_file,result_file,time_allowed)

if __name__ == "__main__":
    main()
