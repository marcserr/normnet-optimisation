import time
import cplex

# This program doesn't need any othen CA2CPLEX python file.

def problem(problem_lp, problem_sol, timeLim):

    #Receives a LP file and solves it. It exports the answer to a .sol file and also shows it.
    #You can specify the time limit with timeLim.

    try:


        m=cplex.Cplex(problem_lp)

        m.parameters.timelimit.set(timeLim)
        m.parameters.mip.limits.treememory.set(2800)
        m.parameters.threads.set(1)
        start_time = time.time()
        m.solve()
        final_time = time.time()-start_time

    # Quitar almohadilla para que cplex vomite los datos
    #    m.solution.write(problem_sol)

        f = open(problem_sol, 'a')

        f.write(100*'_'+'\n')
        f.write('\n\n')
        f.write('Solved in ' + str(final_time) + ' seconds.\n')

        for i in range(len(m.solution.get_values())):
            if m.solution.get_values()[i]!=0:
                f.write(m.variables.get_names()[i]+ ' = '+ m.solution.get_values()[i]+'\n')
        f.close()

    except cplex.exceptions.CplexError:
        print 'Sorry'

if __name__ == "__main__":
    lp_file = 'LPG_1_4.lp'
    result_file = 'result.txt'
    problem(lp_file,result_file,3600)
