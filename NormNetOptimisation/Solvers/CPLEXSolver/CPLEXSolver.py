import time
import cplex

def cplexSolve(problem_lp, problem_sol, timeLim):

    #Receives a LP file and solves it. It exports the answer to a .sol file and also shows it.
    #You can specify the time limit with timeLim.

    try:


        m=cplex.Cplex(problem_lp)

        m.parameters.timelimit.set(timeLim)
        start_time = time.time()
        m.solve()
        final_time = time.time()-start_time

        m.solution.write(problem_sol)



        print 100*'_'
        print '\n'
        print 'Solved in ' + str(final_time) + ' seconds.'

        if 'w_v' in m.variables.get_names():
            print 'Solved VMPNSPLB problem. Here the results:'
        elif 'y' in m.variables.get_names():
            print 'Solved MPNSPLB problem. Here the results:'
        else:
            print 'Solved MPNSP problem. Here the results:'

       for varName, varValue in enumerate(m.solution.get_values()):
            if varValue!=0:
                print str(varName)+' = '+str(varValue)

    except cplex.exceptions.CplexError:
        print 'Sorry'


cplexSolve('problem.lp','problem.sol',30)
